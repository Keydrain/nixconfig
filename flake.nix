{
  description = "NixOS system configuration for multiple computers";

  outputs = inputs @ {
    self,
    agenix,
    dotfiles,
    flake-utils-plus,
    home-manager,
    hosts,
    nixos-generators,
    nixos-hardware,
    nixpkgs,
    nur,
    # peerix,
    U6143_ssd1306,
    ...
  }: let
    inherit (builtins) removeAttrs;
    inherit (flake-utils-plus.lib) exportOverlays exportPackages;

    lib = nixpkgs.lib;
    bundles = import ./bundles.nix {
      inherit flake-utils-plus home-manager dotfiles lib;
    };
    docker = let
      pkgs = nixpkgs.legacyPackages.x86_64-linux;
    in
      import ./docker {inherit pkgs;};
  in
    with bundles.nixosModules;
    with bundles.homeModules;
    with docker;
      flake-utils-plus.lib.mkFlake {
        inherit self inputs;
        inherit (bundles) nixosModules homeModules;

        supportedSystems = ["aarch64-linux" "x86_64-linux"];

        overlays.default = import ./overlays {inherit (self) inputs;};
        sharedOverlays = [self.overlays.default agenix.overlays.default];

        channelsConfig = {
          allowUnfreePredicate = pkg:
            builtins.elem (nixpkgs.lib.getName pkg) [
              "broadcom-sta"
              "cnijfilter2"
              "discord"
              "google-chrome"
              "honey"
              "hplip"
              "memtest86-efi"
              "minecraft-launcher"
              "notable"
              "nvidia-settings"
              "nvidia-x11"
              "obsidian"
              "optifine"
              "slack"
              "steam"
              "steamcmd"
              "steam-original"
              "steam-run"
              "steam-runtime"
              "todoist-electron-8.9.3"
              "todoist-electron"
              "xow_dongle-firmware"
              "zerotierone"
              "zoom"
            ];
          permittedInsecurePackages = [
            "electron-19.1.9"
            # (╯°□°)╯︵ ┻━┻
          ];
        };

        channels = {
          nixpkgs.overlaysBuilder = channels: [
            (_: _: {
              stable-2003 = channels.nixpkgs-2003;
              stable-2009 = channels.nixpkgs-2009;
              stable-2105 = channels.nixpkgs-2105;
              stable-2111 = channels.nixpkgs-2111;
              stable-2205 = channels.nixpkgs-2205;
              stable-2211 = channels.nixpkgs-2211;
              stable-2305 = channels.nixpkgs-2305;
              stable-2311 = channels.nixpkgs-2311;
              stable-2405 = channels.nixpkgs-2405;
              stable = channels.nixpkgs;
              unstable = channels.nixpkgs-unstable;
              master = channels.nixpkgs-master;
            })
          ];
          nixpkgs-unstable.overlaysBuilder = channels: [
            (_: _: {
              stable = channels.nixpkgs-unstable;
              unstable = channels.nixpkgs-master;
            })
          ];
          nixpkgs-master.overlaysBuilder = channels: [(_: _: {stable = channels.nixpkgs-master;})];
        };

        outputsBuilder = channels: {
          packages = exportPackages self.overlays channels;
        };

        packages = let
          modules = [
            agenix.nixosModules.default
            ./machines/install-iso
            ./users/nixos/settings.nix
            {
              home-manager.users.nixos = {...}: {
                imports = [nur.hmModules.nur ./users/nixos/home.nix];
              };
            }
          ];
        in {
          aarch64-linux = let
            pkgs = nixpkgs.legacyPackages.aarch64-linux;
          in {
            install-iso = nixos-generators.nixosGenerate {
              pkgs = pkgs;
              modules = modules ++ bundles.nixosGuiDefaults;
              format = "install-iso";
            };
            raspberry-pi-installer = nixos-generators.nixosGenerate {
              pkgs = pkgs;
              modules =
                modules
                ++ [{sdImage.compressImage = false;}]
                ++ bundles.nixosHeadlessDefaults;
              format = "sd-aarch64-installer";
            };
          };
          x86_64-linux = let
            pkgs = nixpkgs.legacyPackages.x86_64-linux;
          in {
            install-iso = nixos-generators.nixosGenerate {
              pkgs = pkgs;
              modules = modules ++ bundles.nixosGuiDefaults;
              format = "install-iso";
            };
            haproxy = docker.haproxy;
            nginx = docker.nginx;
            haproxy-k3s = docker.haproxy-k3s;
          };
        };

        hostDefaults = {
          channelName = "nixpkgs";
          system = "x86_64-linux";
          modules = [
            agenix.nixosModules.default
            nur.nixosModules.nur
            hosts.nixosModule
            {
              networking.stevenBlackHosts = {
                enable = true;
                blockGambling = true;
                blockPorn = true;
              };
            }
          ];
        };

        hosts = {
          Frampt = {
            modules =
              [
                nixos-hardware.nixosModules.dell-xps-13-9360
                nixos-hardware.nixosModules.common-pc-laptop-ssd
                ./machines/Frampt
                ./users/clint/settings.nix
                {
                  home-manager.users.clint = {...}: {
                    imports = [nur.hmModules.nur ./users/clint/home.nix];
                  };
                }
              ]
              ++ bundles.nixosGuiDefaults;
          };
          BiggerChungus = {
            modules =
              [
                nixos-hardware.nixosModules.common-cpu-intel
                nixos-hardware.nixosModules.common-gpu-nvidia
                nixos-hardware.nixosModules.common-pc-ssd
                nixos-hardware.nixosModules.common-pc
                ./machines/BiggerChungus
                ./users/dylan/settings.nix
                {
                  home-manager.users.dylan = {...}: {
                    imports = [nur.hmModules.nur ./users/dylan/home.nix];
                  };
                }
              ]
              ++ bundles.nixosGuiDefaults;
          };
          Bluestar = {
            modules =
              [
                nixos-hardware.nixosModules.dell-xps-13-9360
                nixos-hardware.nixosModules.common-pc-laptop-ssd
                ./machines/Bluestar
                ./users/darlene/settings.nix
                {
                  home-manager.users.darlene = {...}: {
                    imports = [nur.hmModules.nur ./users/darlene/home.nix];
                  };
                }
              ]
              ++ bundles.nixosGuiDefaults;
          };
          Hal = {
            modules =
              [
                nixos-hardware.nixosModules.dell-xps-13-9360
                nixos-hardware.nixosModules.common-pc-laptop-ssd
                ./machines/Hal
                ./users/marion/settings.nix
                {
                  home-manager.users.marion = {...}: {
                    imports = [nur.hmModules.nur ./users/marion/home.nix];
                  };
                }
              ]
              ++ bundles.nixosGuiDefaults;
          };
          Ornstein = {
            system = "aarch64-linux";
            modules =
              [
                {
                  networking = {
                    hostName = "Ornstein";
                    interfaces.eth0.ipv4.addresses = [
                      {
                        address = "172.24.1.1";
                        prefixLength = 32;
                      }
                    ];
                    stevenBlackHosts = {blockSocial = true;};
                  };
                  keydrain = {
                    openssh.passwordAuth = true;
                    k3s.role = "server";
                  };
                }
                nixos-hardware.nixosModules.raspberry-pi-4
                ./machines/raspberry-pi
                ./users/knight/settings.nix
                {
                  home-manager.users.knight = {...}: {
                    imports = [nur.hmModules.nur ./users/knight/home.nix];
                  };
                }
              ]
              ++ bundles.nixosHeadlessDefaults;
          };
          Artorias = {
            system = "aarch64-linux";
            modules =
              [
                {
                  networking = {
                    hostName = "Artorias";
                    interfaces.eth0.ipv4.addresses = [
                      {
                        address = "172.24.1.2";
                        prefixLength = 32;
                      }
                    ];
                    stevenBlackHosts = {blockSocial = true;};
                  };
                  keydrain = {
                    openssh.passwordAuth = true;
                    k3s.role = "server";
                  };
                }
                nixos-hardware.nixosModules.raspberry-pi-4
                ./machines/raspberry-pi
                ./users/knight/settings.nix
                {
                  home-manager.users.knight = {...}: {
                    imports = [nur.hmModules.nur ./users/knight/home.nix];
                  };
                }
              ]
              ++ bundles.nixosHeadlessDefaults;
          };
          Ciaran = {
            system = "aarch64-linux";
            modules =
              [
                {
                  networking = {
                    hostName = "Ciaran";
                    interfaces.eth0.ipv4.addresses = [
                      {
                        address = "172.24.1.3";
                        prefixLength = 32;
                      }
                    ];
                    stevenBlackHosts = {blockSocial = true;};
                  };
                  keydrain.openssh.passwordAuth = true;
                }
                nixos-hardware.nixosModules.raspberry-pi-4
                ./machines/raspberry-pi
                ./users/knight/settings.nix
                {
                  home-manager.users.knight = {...}: {
                    imports = [nur.hmModules.nur ./users/knight/home.nix];
                  };
                }
              ]
              ++ bundles.nixosHeadlessDefaults;
          };
          Gough = {
            system = "aarch64-linux";
            modules =
              [
                {
                  networking = {
                    hostName = "Gough";
                    interfaces.eth0.ipv4.addresses = [
                      {
                        address = "172.24.1.4";
                        prefixLength = 32;
                      }
                    ];
                    stevenBlackHosts = {blockSocial = true;};
                  };
                  keydrain.openssh.passwordAuth = true;
                }
                nixos-hardware.nixosModules.raspberry-pi-4
                ./machines/raspberry-pi
                ./users/knight/settings.nix
                {
                  home-manager.users.knight = {...}: {
                    imports = [nur.hmModules.nur ./users/knight/home.nix];
                  };
                }
              ]
              ++ bundles.nixosHeadlessDefaults;
          };
          Smough = {
            system = "aarch64-linux";
            modules =
              [
                {
                  networking = {
                    hostName = "Smough";
                    interfaces.eth0.ipv4.addresses = [
                      {
                        address = "172.24.1.5";
                        prefixLength = 32;
                      }
                    ];
                    stevenBlackHosts = {blockSocial = true;};
                  };
                  keydrain.openssh.passwordAuth = true;
                }
                nixos-hardware.nixosModules.raspberry-pi-4
                ./machines/raspberry-pi
                ./users/knight/settings.nix
                {
                  home-manager.users.knight = {...}: {
                    imports = [nur.hmModules.nur ./users/knight/home.nix];
                  };
                }
              ]
              ++ bundles.nixosHeadlessDefaults;
          };
          Gwyn = {
            modules =
              [
                {
                  networking = {
                    hostName = "Gwyn";
                    interfaces.enp88s0 = {
                      useDHCP = true;
                      ipv4.addresses = [
                        {
                          address = "172.24.2.1";
                          prefixLength = 32;
                        }
                      ];
                    };
                    stevenBlackHosts = {blockSocial = true;};
                  };
                  keydrain = {
                    openssh.passwordAuth = true;
                    k3s.role = "server";
                  };
                }
                nixos-hardware.nixosModules.common-cpu-intel
                nixos-hardware.nixosModules.common-pc-ssd
                nixos-hardware.nixosModules.common-pc
                ./machines/intel-nuc
                ./users/lord/settings.nix
                {
                  home-manager.users.lord = {...}: {
                    imports = [nur.hmModules.nur ./users/lord/home.nix];
                  };
                }
              ]
              ++ bundles.nixosHeadlessDefaults;
          };
          Gwynevere = {
            modules =
              [
                {
                  networking = {
                    hostName = "Gwynevere";
                    interfaces.enp88s0 = {
                      useDHCP = true;
                      ipv4.addresses = [
                        {
                          address = "172.24.2.2";
                          prefixLength = 32;
                        }
                      ];
                    };
                    stevenBlackHosts = {blockSocial = true;};
                  };
                  keydrain.openssh.passwordAuth = true;
                }
                nixos-hardware.nixosModules.common-cpu-intel
                nixos-hardware.nixosModules.common-pc-ssd
                nixos-hardware.nixosModules.common-pc
                ./machines/intel-nuc
                ./users/lord/settings.nix
                {
                  home-manager.users.lord = {...}: {
                    imports = [nur.hmModules.nur ./users/lord/home.nix];
                  };
                }
              ]
              ++ bundles.nixosHeadlessDefaults;
          };
          Gwyndolin = {
            modules =
              [
                {
                  networking = {
                    hostName = "Gwyndolin";
                    interfaces.enp89s0 = {
                      useDHCP = true;
                      ipv4.addresses = [
                        {
                          address = "172.24.2.3";
                          prefixLength = 32;
                        }
                      ];
                    };
                    stevenBlackHosts = {blockSocial = true;};
                  };
                  keydrain.openssh.passwordAuth = true;
                }
                nixos-hardware.nixosModules.common-cpu-intel
                nixos-hardware.nixosModules.common-pc-ssd
                nixos-hardware.nixosModules.common-pc
                ./machines/intel-nuc
                ./users/lord/settings.nix
                {
                  home-manager.users.lord = {...}: {
                    imports = [nur.hmModules.nur ./users/lord/home.nix];
                  };
                }
              ]
              ++ bundles.nixosHeadlessDefaults;
          };
        };
      };

  inputs = {
    # NixOS Flakes
    nixpkgs-2003.url = "github:nixos/nixpkgs/nixos-20.03";
    nixpkgs-2009.url = "github:nixos/nixpkgs/nixos-20.09";
    nixpkgs-2105.url = "github:nixos/nixpkgs/nixos-21.05";
    nixpkgs-2111.url = "github:nixos/nixpkgs/nixos-21.11";
    nixpkgs-2205.url = "github:nixos/nixpkgs/nixos-22.05";
    nixpkgs-2211.url = "github:nixos/nixpkgs/nixos-22.11";
    nixpkgs-2305.url = "github:nixos/nixpkgs/nixos-23.05";
    nixpkgs-2311.url = "github:nixos/nixpkgs/nixos-23.11";
    nixpkgs-2405.url = "github:nixos/nixpkgs/nixos-24.05";
    nixpkgs.url = "github:nixos/nixpkgs/nixos-24.05";
    nixpkgs-unstable.url = "github:nixos/nixpkgs/nixos-unstable";
    nixpkgs-master.url = "github:nixos/nixpkgs/master";
    nixos-hardware.url = "github:nixos/nixos-hardware";

    # Nix Community Flakes
    home-manager = {
      url = "github:nix-community/home-manager/release-24.05";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    nixos-generators = {
      url = "github:nix-community/nixos-generators";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    nur.url = "github:nix-community/NUR";

    # Other Flakes
    agenix = {
      # Eventually upgrade to "github:yaxitech/ragenix"
      url = "github:ryantm/agenix";
      inputs = {
        nixpkgs.follows = "nixpkgs";
        home-manager.follows = "home-manager";
        darwin.follows = "";
      };
    };
    flake-utils.url = "github:numtide/flake-utils";
    flake-utils-plus = {
      url = "github:gytis-ivaskevicius/flake-utils-plus";
      inputs.flake-utils.follows = "flake-utils";
    };
    hosts = {
      url = "github:StevenBlack/hosts";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    # peerix = {
    #   url = "github:cid-chan/peerix";
    #   inputs = {
    #     nixpkgs.follows = "nixpkgs";
    #     flake-utils.follows = "flake-utils";
    #   };
    # };

    # Non-flakes
    U6143_ssd1306 = {
      type = "github";
      owner = "UCTRONICS";
      repo = "U6143_ssd1306";
      flake = false;
    };

    # My Flakes
    dotfiles = {
      type = "git";
      url = "https://gitlab.com/Keydrain/dotfiles.git";
      ref = "main";
      submodules = true;
    };
  };
}
