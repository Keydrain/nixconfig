---

### NIX FLAKES ###

.nix-base:
  before_script:
    - echo "sandbox = false" >> /etc/nix/nix.conf
    - echo "sandbox-fallback = false" >> /etc/nix/nix.conf
    - echo "extra-experimental-features = nix-command flakes" >> /etc/nix/nix.conf
    - echo "filter-syscalls = false" >> /etc/nix/nix.conf
    - echo "extra-platforms = $ARCHS" >> /etc/nix/nix.conf
    - echo "system-features = benchmark big-parallel nixos-test recursive-nix kvm" >> /etc/nix/nix.conf
    - nix-channel --add https://nixos.org/channels/nixos-23.05 nixos
    - nix-channel --update
  retry: 1
  rules:
    - changes:
        - docker/**/*
        - extras/**/*
        - machines/**/*
        - modules/**/*
        - overlays/**/*
        - programs/**/*
        - secrets/**/*
        - users/**/*
        - bundles.nix
        - flake.lock
        - flake.nix
        - .gitlab-ci.yml
      when: on_success

flake-check-amd64:
  extends: .nix-base
  needs:
    - deploy-gitlab-runners
  image: nixos/nix:latest-amd64
  variables:
    ARCHS: "aarch64-linux"
  script:
    - nix flake check --verbose --all-systems --show-trace
  tags:
    - anor-londo
    - amd64

flake-check-arm64:
  extends: .nix-base
  needs:
    - deploy-gitlab-runners
  image: nixos/nix:latest-arm64
  variables:
    ARCHS: "x86_64-linux"
  script:
    - nix flake check --verbose --all-systems --show-trace
  tags:
    - anor-londo
    - arm64

### DOCKER ###

check-docker-hub:
  image: alpine:latest
  before_script:
    - apk add curl jq
  script:
    - |
      TOKEN=$(curl "https://auth.docker.io/token?service=registry.docker.io&scope=repository:ratelimitpreview/test:pull" | jq --raw-output .token) && curl --head --header "Authorization: Bearer $TOKEN" "https://registry-1.docker.io/v2/ratelimitpreview/test/manifests/latest" 2>&1
    - |
      TOKEN=$(curl "https://auth.docker.io/token?service=registry.docker.io&scope=repository:ratelimitpreview/test:pull" | jq --raw-output .token) && COUNT=$(curl --head --header "Authorization: Bearer $TOKEN" "https://registry-1.docker.io/v2/ratelimitpreview/test/manifests/latest" | grep remaining | sed 's/[a-z: \-]*\([0-9]*\);[a-z]=.*/\1/g') && if [ "$COUNT" -gt 0 ]; then exit 0; else exit 1; fi
  retry: 1
  tags:
    - anor-londo

docker-build:
  extends: .nix-base
  needs:
    - check-docker-hub
    - flake-check-amd64
    - deploy-gitlab-runners
  image: nixos/nix:latest-amd64
  script:
    - mkdir -p /var/tmp
    - nix-env -iA nixos.skopeo nixos.nix-prefetch-docker nixos.gawk
    - skopeo login --username $CI_REGISTRY_USER --password $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - if [ ! -z "$DEPENDS" ]; then grep -E "${DEPENDS}.*pullImage" docker/default.nix -A 6 > /tmp/snippet.nix; fi
    - if [ ! -z "$DEPENDS" ]; then nix-prefetch-docker --image-name $(grep imageName /tmp/snippet.nix | awk -F'"' '{print $2}') --image-digest $(grep imageDigest /tmp/snippet.nix -A 1 | grep "sha256:" | awk -F'"' '{print $2}') --image-tag $(grep finalImageTag /tmp/snippet.nix | awk -F'"' '{print $2}'); fi
    - nix build .#$DOCKER_IMAGE --verbose --system $ARCH
    - skopeo --insecure-policy copy --retry-times 1 docker-archive:result docker://$CI_REGISTRY_IMAGE/$DOCKER_IMAGE:$CI_COMMIT_BRANCH
    - skopeo --insecure-policy copy --retry-times 1 docker-archive:result docker://$CI_REGISTRY_IMAGE/$DOCKER_IMAGE:$CI_COMMIT_BRANCH-$(date -u +%y.%m)
    - skopeo --insecure-policy copy --retry-times 1 docker-archive:result docker://$CI_REGISTRY_IMAGE/$DOCKER_IMAGE:$CI_COMMIT_BRANCH-$(date -u +%y.%m.%d)
    - skopeo --insecure-policy copy --retry-times 1 docker-archive:result docker://$CI_REGISTRY_IMAGE/$DOCKER_IMAGE:$CI_COMMIT_BRANCH-$(date -u +%y.%m.%d.%H)
    - skopeo --insecure-policy copy --retry-times 1 docker-archive:result docker://$CI_REGISTRY_IMAGE/$DOCKER_IMAGE:$CI_COMMIT_BRANCH-$CI_COMMIT_SHORT_SHA
  parallel:
    matrix:
      - ARCH: [x86_64-linux]
        DOCKER_IMAGE: [haproxy, nginx, haproxy-k3s]
  rules:
    - changes:
        - flake.nix
        - flake.lock
        - docker/**/*
        - .gitlab-ci.yml
      when: on_success
  tags:
    - anor-londo
    - amd64

### APPS ###

.kube-context:
  image: "registry.gitlab.com/gitlab-org/cluster-integration/cluster-applications:v2.0.0"
  before_script:
    - if [ -n "$KUBE_CONTEXT" ]; then kubectl config use-context "$KUBE_CONTEXT"; fi
    - if [ -n "$KUBE_CONTEXT" ]; then gl-ensure-namespace gitlab-agent; fi
    - if [ -n "$KUBE_CONTEXT" ]; then gl-ensure-namespace gitlab-runners; fi
  retry: 1
  tags:
    - anor-londo

.deploy-k8s-resource:
  extends: .kube-context
  needs:
    - test-helmfiles
  environment:
    deployment_tier: production
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      when: on_success

test-helmfiles:
  extends: .kube-context
  needs: [ ]
  script:
    - gl-helmfile --file $CI_PROJECT_DIR/apps/$APP/helmfile.yaml lint
    - gl-helmfile --file $CI_PROJECT_DIR/apps/helmfile-$APP.yaml lint
  parallel:
    matrix:
      - APP: [ddclient, cert-manager, gitlab-runner, metallb, nfs-provisioner, traefik, foundry-vtt, kubernetes-dashboard]

deploy-nfs-provisioner:
  extends: .deploy-k8s-resource
  needs:
    - job: test-helmfiles
      parallel:
        matrix:
          - APP: nfs-provisioner
  environment:
    name: anor-londo/nfs-provisioner
  script:
    - if [ -n "$KUBE_CONTEXT" ]; then gl-ensure-namespace nfs-provisioner; fi
    - gl-helmfile --file $CI_PROJECT_DIR/apps/helmfile-nfs-provisioner.yaml apply --suppress-secrets
    - kubectl rollout status deploy/nfs-provisioner-nfs-subdir-external-provisioner -n nfs-provisioner
    - >-
      kubectl patch storageclass local-path -p '{"metadata": {"annotations":{"storageclass.kubernetes.io/is-default-class":"false"}}}'

deploy-metallb:
  extends: .deploy-k8s-resource
  needs:
    - job: test-helmfiles
      parallel:
        matrix:
          - APP: metallb
  environment:
    name: anor-londo/metallb
  script:
    - if [ -n "$KUBE_CONTEXT" ]; then gl-ensure-namespace metallb-system; fi
    - gl-helmfile --file $CI_PROJECT_DIR/apps/helmfile-metallb.yaml apply --suppress-secrets
    - kubectl rollout status deploy/metallb-controller -n metallb-system
    - kubectl apply -f $CI_PROJECT_DIR/apps/metallb/services-ip-pool.yaml

deploy-traefik:
  extends: .deploy-k8s-resource
  needs:
    - job: test-helmfiles
      parallel:
        matrix:
          - APP: traefik
    - job: deploy-metallb
  environment:
    name: anor-londo/traefik
  script:
    - if [ -n "$KUBE_CONTEXT" ]; then gl-ensure-namespace traefik; fi
    - gl-helmfile --file $CI_PROJECT_DIR/apps/helmfile-traefik.yaml apply --suppress-secrets
    - kubectl rollout status deploy/traefik -n traefik
    - kubectl apply -k $CI_PROJECT_DIR/apps/traefik/

deploy-cert-manager:
  extends: .deploy-k8s-resource
  needs:
    - job: test-helmfiles
      parallel:
        matrix:
          - APP: cert-manager
  environment:
    name: anor-londo/cert-manager
  script:
    - if [ -n "$KUBE_CONTEXT" ]; then gl-ensure-namespace cert-manager; fi
    - kubectl apply -f https://github.com/cert-manager/cert-manager/releases/download/v1.13.2/cert-manager.crds.yaml
    - gl-helmfile --file $CI_PROJECT_DIR/apps/helmfile-cert-manager.yaml apply --suppress-secrets
    - kubectl rollout status deploy/jetstack-cert-manager -n cert-manager
    - kubectl rollout status deploy/jetstack-cert-manager-cainjector -n cert-manager
    - kubectl rollout status deploy/jetstack-cert-manager-webhook -n cert-manager
    - kubectl apply -k $CI_PROJECT_DIR/apps/cert-manager/

deploy-demo-nginx:
  extends: .deploy-k8s-resource
  needs:
    - deploy-traefik
    - deploy-cert-manager
  environment:
    name: anor-londo/demo-nginx
  script:
    - kubectl apply -k $CI_PROJECT_DIR/apps/demo-nginx/
    - kubectl rollout status deploy/nginx -n default

deploy-gitlab-runners:
  extends: .deploy-k8s-resource
  needs:
    - job: test-helmfiles
      parallel:
        matrix:
          - APP: gitlab-runner
  environment:
    name: anor-londo/gitlab-runners
  script:
    - if [ -n "$KUBE_CONTEXT" ]; then gl-ensure-namespace gitlab-runners; fi
    - gl-helmfile --file $CI_PROJECT_DIR/apps/helmfile-gitlab-runner.yaml apply --suppress-secrets
    - kubectl rollout status deploy/gitlab-runner-amd64 -n gitlab-runners
    - kubectl rollout status deploy/gitlab-runner-arm64 -n gitlab-runners

deploy-kubernetes-dashboard:
  extends: .deploy-k8s-resource
  needs:
    - job: test-helmfiles
      parallel:
        matrix:
          - APP: kubernetes-dashboard
    - job: deploy-traefik
    - job: deploy-cert-manager
    - job: deploy-metallb
  environment:
    name: anor-londo/kubernetes-dashboard
  script:
    - if [ -n "$KUBE_CONTEXT" ]; then gl-ensure-namespace kubernetes-dashboard; fi
    - gl-helmfile --file $CI_PROJECT_DIR/apps/helmfile-kubernetes-dashboard.yaml apply --suppress-secrets
    - kubectl rollout status deploy/k8s-dashboard-kubernetes-dashboard -n kubernetes-dashboard
    - kubectl -n kubernetes-dashboard patch svc k8s-dashboard-kubernetes-dashboard -p '{"spec":{"externalTrafficPolicy":"Local"}}'

deploy-foundry-vtt:
  extends: .deploy-k8s-resource
  needs:
    - job: test-helmfiles
      parallel:
        matrix:
          - APP: foundry-vtt
    - job: deploy-traefik
    - job: deploy-cert-manager
    - job: deploy-metallb
  environment:
    name: anor-londo/foundry-vtt
  script:
    - gl-helmfile --file $CI_PROJECT_DIR/apps/helmfile-foundry-vtt.yaml apply --suppress-secrets
    - kubectl rollout status deploy/foundry-vtt -n default

# deploy-minecraft:
#   extends: .deploy-helmfile
#   needs:
#     - deploy-nfs-provisioner
#     - deploy-metallb
#   environment:
#     name: anor-londo/minecraft
#   script:
#     - if [ -n "$KUBE_CONTEXT" ]; then gl-ensure-namespace games; fi
#     - gl-helmfile --file $CI_PROJECT_DIR/apps/helmfile-minecraft.yaml apply --suppress-secrets
#     - kubectl rollout status deploy/minecraft-minecraft -n games

# deploy-valheim:
#   extends: .deploy-helmfile
#   needs:
#     - deploy-nfs-provisioner
#     - deploy-metallb
#   environment:
#     name: anor-londo/valheim
#   script:
#     - if [ -n "$KUBE_CONTEXT" ]; then gl-ensure-namespace games; fi
#     - gl-helmfile --file $CI_PROJECT_DIR/apps/helmfile-valheim.yaml apply --suppress-secrets
#     - kubectl rollout status deploy/valheim-server -n games

# # stop-helmfile:
# #   extends: [.kube-context]
# #   needs: [test-helmfile]
# #   environment:
# #     name: anor-londo/helmfile
# #     deployment_tier: production
# #     action: stop
# #   script:
# #     - gl-helmfile --file $CI_PROJECT_DIR/apps/helmfile.yaml destroy
# #   rules:
# #     - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $STOP_HELMFILE
# #       changes:
# #         - apps/**/*
# #         - .gitlab-ci.yml
# #       when: manual
