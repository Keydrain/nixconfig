{
  config,
  lib,
  pkgs,
  ...
}:
with lib; let
  cfg = config.keydrain.obs-studio;
in {
  options = {
    keydrain.obs-studio.enable =
      mkEnableOption "Enable keydrain's obs-studio configuration";
  };

  config = mkIf cfg.enable {
    home.packages = with pkgs; [obs-studio obs-studio-plugins.wlrobs];
  };
}
