{
  config,
  lib,
  pkgs,
  ...
}:
with lib; let
  cfg = config.keydrain.zsh;
in {
  options = {
    keydrain.zsh.enable = mkEnableOption "Enable keydrain's zsh configuration";
  };

  config = mkIf cfg.enable {
    programs.zsh = {
      enable = true;
      shellAliases = {
        nixconf = "cd ~/git/nixconfig";
        nixupdate = "( nixconf && nix flake update )";
        nixbuild = "sudo nixos-rebuild --verbose ";
        nixup = "nixupdate && nixbuild switch";
        nixsync = "sudo systemctl start nixconfig && sleep 1 && nixbuild switch";
      };
    };
  };
}
