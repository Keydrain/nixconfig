{
  config,
  lib,
  pkgs,
  ...
}:
with lib; let
  cfg = config.keydrain.supertux;
in {
  options = {
    keydrain.supertux.enable =
      mkEnableOption "Enable keydrain's supertux configuration";
  };

  config = mkIf cfg.enable {home.packages = with pkgs; [superTux];};
}
