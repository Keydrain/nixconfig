{
  config,
  lib,
  pkgs,
  ...
}:
with lib; let
  cfg = config.keydrain.discord;
in {
  options = {
    keydrain.discord.enable =
      mkEnableOption "Enable keydrain's discord configuration";
  };

  config = mkIf cfg.enable {
    home.packages = with pkgs; [master.discord];

    # May need to add overlay for discord versioning
    # nixpkgs.overlays = [
    #   # (self: super: { discord = super.discord.overrideAttrs (_: { src = builtins.fetchTarball https://dl.discordapp.net/apps/linux/0.0.15/discord-0.0.15.tar.gz; });})
    # ];
  };
}
