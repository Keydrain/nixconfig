{
  config,
  lib,
  pkgs,
  ...
}:
with lib; let
  cfg = config.keydrain.starship;
in {
  options = {
    keydrain.starship.enable =
      mkEnableOption "Enable keydrain's starship configuration";
  };

  config = mkIf cfg.enable {home.packages = with pkgs; [starship];};
}
