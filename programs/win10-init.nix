{
  config,
  lib,
  pkgs,
  ...
}:
with lib; let
  cfg = config.keydrain.win10-init;
in {
  options = {
    keydrain.win10-init.enable =
      mkEnableOption "Enable keydrain's win10-init configuration";
  };

  config = mkIf cfg.enable {
    home.file = {
      Win10-Init = {
        executable = true;
        source = ../extras/win10-init/Win10-Init.sh;
        target = ".local/bin/win10-init";
      };
    };
  };
}
