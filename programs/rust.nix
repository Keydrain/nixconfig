{
  config,
  lib,
  pkgs,
  ...
}:
with lib; let
  cfg = config.keydrain.rust;
in {
  options = {
    keydrain.rust.enable =
      mkEnableOption "Enable keydrain's rust configuration";
  };

  config = mkIf cfg.enable {home.packages = with pkgs; [rustc rustfmt];};
}
