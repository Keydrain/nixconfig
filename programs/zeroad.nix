{
  config,
  lib,
  pkgs,
  ...
}:
with lib; let
  cfg = config.keydrain.zeroad;
in {
  options = {
    keydrain.zeroad.enable =
      mkEnableOption "Enable keydrain's zeroad configuration";
  };

  config = mkIf cfg.enable {home.packages = with pkgs; [zeroad];};
}
