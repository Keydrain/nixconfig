{
  config,
  lib,
  pkgs,
  ...
}:
with lib; let
  cfg = config.keydrain.zoom;
in {
  options = {
    keydrain.zoom.enable =
      mkEnableOption "Enable keydrain's zoom configuration";
  };

  config = mkIf cfg.enable {home.packages = with pkgs; [zoom-us];};
}
