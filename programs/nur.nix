{
  config,
  lib,
  pkgs,
  ...
}:
with lib; let
  cfg = config.keydrain.nur;
in {
  options = {
    keydrain.nur.enable = mkEnableOption "Enable keydrain's nur configuration";
  };

  config = mkIf cfg.enable {
    home.file = {
      ".config/nixpkgs/config.nix" = {
        text = ''
          {
            allowUnfree = true;
            packageOverrides = pkgs: {
              nur = import (builtins.fetchTarball "https://github.com/nix-community/NUR/archive/master.tar.gz") {
                inherit pkgs;
              };
            };
          }
        '';
      };
    };
  };
}
