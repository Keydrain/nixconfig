{
  config,
  lib,
  pkgs,
  ...
}:
with lib; let
  cfg = config.keydrain.easyeffects;
in {
  options = {
    keydrain.easyeffects.enable =
      mkEnableOption "Enable keydrain's easyeffects configuration";
  };

  config = mkIf cfg.enable {home.packages = with pkgs; [easyeffects];};
}
