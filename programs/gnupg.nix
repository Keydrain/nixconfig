{
  config,
  lib,
  pkgs,
  ...
}:
with lib; let
  cfg = config.keydrain.gnupg;
in {
  options = {
    keydrain.gnupg.enable =
      mkEnableOption "Enable keydrain's gnupg configuration";
  };

  config = mkIf cfg.enable {
    home.packages = with pkgs; [pinentry];
    programs.gpg.enable = true;
    services.gpg-agent = {
      enable = true;
      enableSshSupport = true;
      pinentryPackage = null;
      extraConfig = ''
        pinentry-program ${pkgs.pinentry}/bin/pinentry
      '';
    };
  };
}
