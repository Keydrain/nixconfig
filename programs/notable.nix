{
  config,
  lib,
  pkgs,
  ...
}:
with lib; let
  cfg = config.keydrain.notable;
in {
  options = {
    keydrain.notable.enable =
      mkEnableOption "Enable keydrain's notable configuration";
  };

  config = mkIf cfg.enable {home.packages = with pkgs; [master.notable];};
}
