{
  config,
  lib,
  pkgs,
  ...
}:
with lib; let
  cfg = config.keydrain.htop;
in {
  options = {
    keydrain.htop.enable =
      mkEnableOption "Enable keydrain's htop configuration";
  };

  config = mkIf cfg.enable {home.packages = with pkgs; [htop];};
}
