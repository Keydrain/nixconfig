{
  config,
  lib,
  pkgs,
  ...
}:
with lib; let
  cfg = config.keydrain.parted;
in {
  options = {
    keydrain.parted.enable =
      mkEnableOption "Enable keydrain's parted configuration";
    keydrain.parted.gui = mkOption {
      type = types.bool;
      default = false;
      description = "Enable gparted for keydrain's parted configuration";
    };
  };

  config = mkIf cfg.enable {
    home.packages = with pkgs;
      (
        if (cfg.gui)
        then [gparted]
        else []
      )
      ++ [parted gparted smartmontools];
  };
}
