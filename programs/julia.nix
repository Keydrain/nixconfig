{
  config,
  lib,
  pkgs,
  ...
}:
with lib; let
  cfg = config.keydrain.julia;
in {
  options = {
    keydrain.julia.enable =
      mkEnableOption "Enable keydrain's julia configuration";
  };

  config =
    mkIf cfg.enable {home.packages = with pkgs; [julia-stable julia-mono];};
}
