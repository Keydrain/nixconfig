{
  config,
  lib,
  pkgs,
  ...
}:
with lib; let
  cfg = config.keydrain.python;
in {
  options = {
    keydrain.python.enable =
      mkEnableOption "Enable keydrain's python configuration";
  };

  config = mkIf cfg.enable {
    home.packages = with pkgs; [
      pypi2nix
      (python3.withPackages (ps: with ps; [pip]))
    ];
  };
}
