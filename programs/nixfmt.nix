{
  config,
  lib,
  pkgs,
  ...
}:
with lib; let
  cfg = config.keydrain.nixfmt;
in {
  options = {
    keydrain.nixfmt.enable =
      mkEnableOption "Enable keydrain's nixfmt configuration";
  };

  config = mkIf cfg.enable {
    home.packages = with pkgs; [nixfmt];

    programs.vim = {
      extraConfig = ''
        " Auto format files on save
        autocmd BufWritePost *.nix silent! !nixfmt %:p:S
      '';
    };
  };
}
