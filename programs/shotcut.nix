{
  config,
  lib,
  pkgs,
  ...
}:
with lib; let
  cfg = config.keydrain.shotcut;
in {
  options = {
    keydrain.shotcut.enable =
      mkEnableOption "Enable keydrain's shotcut configuration";
  };

  config = mkIf cfg.enable {home.packages = with pkgs; [shotcut];};
}
