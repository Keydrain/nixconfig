{
  config,
  lib,
  pkgs,
  ...
}:
with lib; let
  cfg = config.keydrain.gource;
in {
  options = {
    keydrain.gource.enable =
      mkEnableOption "Enable keydrain's gource configuration";
  };

  config = mkIf cfg.enable {
    home.packages = with pkgs; [ffmpeg-full gource];

    home.file = {
      gource-visualize-repo = {
        executable = true;
        source = ../extras/gource/visualize.sh;
        target = ".local/bin/visualize-repo";
      };
    };
  };
}
