{
  config,
  lib,
  pkgs,
  ...
}:
with lib; let
  cfg = config.keydrain.dmidecode;
in {
  options = {
    keydrain.dmidecode.enable = mkEnableOption "Enable keydrain's dmidecode configuration";
  };

  config = mkIf cfg.enable {home.packages = with pkgs; [dmidecode];};
}
