{
  config,
  lib,
  pkgs,
  ...
}:
with lib; let
  cfg = config.keydrain.gthumb;
in {
  options = {
    keydrain.gthumb.enable =
      mkEnableOption "Enable keydrain's gthumb configuration";
  };

  config = mkIf cfg.enable {home.packages = with pkgs; [gthumb];};
}
