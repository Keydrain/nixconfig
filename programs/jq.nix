{
  config,
  lib,
  ...
}:
with lib; let
  cfg = config.keydrain.jq;
in {
  options = {
    keydrain.jq.enable = mkEnableOption "Enable keydrain's jq configuration";
  };

  config = mkIf cfg.enable {programs.jq.enable = true;};
}
