{
  config,
  lib,
  pkgs,
  ...
}:
with lib; let
  cfg = config.keydrain.alejandra;
in {
  options = {
    keydrain.alejandra.enable =
      mkEnableOption "Enable keydrain's alejandra configuration";
  };

  config = mkIf cfg.enable {
    home.packages = with pkgs; [alejandra];

    programs.vim = {
      extraConfig = ''
        " Auto format files on save
        autocmd BufWritePost,FileWritePost *.nix silent! !alejandra -q %:p:S
      '';
    };
  };
}
