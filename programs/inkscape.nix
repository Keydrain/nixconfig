{
  config,
  lib,
  pkgs,
  ...
}:
with lib; let
  cfg = config.keydrain.inkscape;
in {
  options = {
    keydrain.inkscape.enable =
      mkEnableOption "Enable keydrain's inkscape configuration";
  };

  config = mkIf cfg.enable {home.packages = with pkgs; [inkscape];};
}
