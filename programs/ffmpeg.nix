{
  config,
  lib,
  pkgs,
  ...
}:
with lib; let
  cfg = config.keydrain.ffmpeg;
in {
  options = {
    keydrain.ffmpeg.enable =
      mkEnableOption "Enable keydrain's ffmpeg configuration";
  };

  config = mkIf cfg.enable {
    home.packages = with pkgs; [ffmpeg-full vid-stab];

    home.file = {
      ffmpeg-compare-video = {
        executable = true;
        source = ../extras/ffmpeg/compare-video.sh;
        target = ".local/bin/compare-video";
      };
      ffmpeg-stabilize-video = {
        executable = true;
        source = ../extras/ffmpeg/stabilize-video.sh;
        target = ".local/bin/stabilize-video";
      };
    };
  };
}
