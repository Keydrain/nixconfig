{
  config,
  lib,
  pkgs,
  ...
}:
with lib; let
  cfg = config.keydrain.pciutils;
in {
  options = {
    keydrain.pciutils.enable =
      mkEnableOption "Enable keydrain's pciutils configuration";
  };

  config = mkIf cfg.enable {home.packages = with pkgs; [pciutils];};
}
