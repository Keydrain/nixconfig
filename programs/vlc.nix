{
  config,
  lib,
  pkgs,
  ...
}:
with lib; let
  cfg = config.keydrain.vlc;
in {
  options = {
    keydrain.vlc.enable = mkEnableOption "Enable keydrain's vlc configuration";
  };

  config = mkIf cfg.enable {home.packages = with pkgs; [vlc];};
}
