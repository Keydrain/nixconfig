{
  config,
  lib,
  pkgs,
  ...
}:
with lib; let
  cfg = config.keydrain.libreoffice;
in {
  options = {
    keydrain.libreoffice.enable =
      mkEnableOption "Enable keydrain's libreoffice configuration";
  };

  config = mkIf cfg.enable {home.packages = with pkgs; [libreoffice];};
}
