{
  config,
  lib,
  pkgs,
  ...
}:
with lib; let
  cfg = config.keydrain.minecraft;
in {
  options = {
    keydrain.minecraft.enable =
      mkEnableOption "Enable keydrain's minecraft configuration";
  };

  config = mkIf cfg.enable {
    home.packages = with pkgs; [prismlauncher unstable.optifine];
  };
}
