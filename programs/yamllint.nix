{
  config,
  lib,
  pkgs,
  ...
}:
with lib; let
  cfg = config.keydrain.yamllint;
in {
  options = {
    keydrain.yamllint.enable =
      mkEnableOption "Enable keydrain's yamllint configuration";
  };

  config = mkIf cfg.enable {home.packages = with pkgs; [yamllint];};
}
