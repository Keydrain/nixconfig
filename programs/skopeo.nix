{
  config,
  lib,
  pkgs,
  ...
}:
with lib; let
  cfg = config.keydrain.skopeo;
in {
  options = {
    keydrain.skopeo.enable =
      mkEnableOption "Enable keydrain's skopeo configuration";
  };

  config = mkIf cfg.enable {home.packages = with pkgs; [skopeo];};
}
