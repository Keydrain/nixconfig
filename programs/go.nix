{
  config,
  lib,
  pkgs,
  ...
}:
with lib; let
  cfg = config.keydrain.go;
in {
  options = {
    keydrain.go.enable = mkEnableOption "Enable keydrain's go configuration";
  };

  config = mkIf cfg.enable {home.packages = with pkgs; [go];};
}
