{
  config,
  lib,
  pkgs,
  ...
}:
with lib; let
  cfg = config.keydrain.tree;
in {
  options = {
    keydrain.tree.enable =
      mkEnableOption "Enable keydrain's tree configuration";
  };

  config = mkIf cfg.enable {
    home.packages = with pkgs; [tree];

    programs = {
      zsh.shellAliases =
        if (config.programs.zsh.enable)
        then {
          t = "tree -pshDFC --du";
          t2 = "t -L 2";
          t3 = "t -L 3";
          t4 = "t -L 4";
          t5 = "t -L 5";
          t6 = "t -L 6";
          t7 = "t -L 7";
          t8 = "t -L 8";
          t9 = "t -L 9";
        }
        else {};
    };
  };
}
