{
  config,
  lib,
  pkgs,
  ...
}:
with lib; let
  cfg = config.keydrain.chuck;
in {
  options = {
    keydrain.chuck.enable =
      mkEnableOption "Enable keydrain's chuck configuration";
  };

  config =
    mkIf cfg.enable {home.packages = with pkgs; [chuck miniaudicle];};
}
