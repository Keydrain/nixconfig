{
  config,
  lib,
  pkgs,
  ...
}:
with lib; let
  cfg = config.keydrain.obsidian;
in {
  options = {
    keydrain.obsidian.enable =
      mkEnableOption "Enable keydrain's obsidian configuration";
  };

  config = mkIf cfg.enable {home.packages = with pkgs; [master.obsidian];};
}
