{
  config,
  lib,
  pkgs,
  ...
}:
with lib; let
  cfg = config.keydrain.temp;
in {
  options = {
    keydrain.temp.enable =
      mkEnableOption "Enable keydrain's temp configuration";
  };

  config = mkIf cfg.enable {
    environment.systemPackages = with pkgs; [thing];

    hardware = {
      opengl.enable = true;
      nvidia = {
        modesetting.enable = true;
        prime.offload.enable = false;
      };
    };
    services.xserver.videoDrivers = ["nvidia"];
  };
}
