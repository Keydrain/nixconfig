{
  config,
  lib,
  pkgs,
  ...
}:
with lib; let
  cfg = config.keydrain.slack;
  slack = pkgs.slack.overrideAttrs (old: {
    installPhase =
      old.installPhase
      + ''
        rm $out/bin/slack

        makeWrapper $out/lib/slack/slack $out/bin/slack \
          --prefix XDG_DATA_DIRS : $GSETTINGS_SCHEMAS_PATH \
          --prefix PATH : ${makeBinPath [pkgs.xdg-utils]} \
          --add-flags "--ozone-platform=wayland --enable-features=UseOzonePlatform,WebRTCPipeWireCapturer"
      '';
  });
in {
  options = {
    keydrain.slack.enable =
      mkEnableOption "Enable keydrain's slack configuration";
  };

  config = mkIf cfg.enable {home.packages = with pkgs; [slack-dark];};
}
