{
  config,
  lib,
  pkgs,
  ...
}:
with lib; let
  cfg = config.keydrain.usbutils;
in {
  options = {
    keydrain.usbutils.enable =
      mkEnableOption "Enable keydrain's usbutils configuration";
  };

  config = mkIf cfg.enable {home.packages = with pkgs; [usbutils];};
}
