{
  config,
  lib,
  pkgs,
  ...
}:
with lib; let
  cfg = config.keydrain.rpi-imager;
in {
  options = {
    keydrain.rpi-imager.enable =
      mkEnableOption "Enable keydrain's rpi-imager configuration";
  };

  config = mkIf cfg.enable {home.packages = with pkgs; [rpi-imager];};
}
