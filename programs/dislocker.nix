{
  config,
  lib,
  pkgs,
  ...
}:
with lib; let
  cfg = config.keydrain.dislocker;
in {
  options = {
    keydrain.dislocker.enable =
      mkEnableOption "Enable keydrain's dislocker configuration";
  };

  config = mkIf cfg.enable {home.packages = with pkgs; [dislocker];};
}
