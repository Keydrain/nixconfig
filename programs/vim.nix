{
  config,
  lib,
  pkgs,
  ...
}:
with lib; let
  cfg = config.keydrain.vim;
in {
  options = {
    keydrain.vim.enable = mkEnableOption "Enable keydrain's vim configuration";
  };

  config = mkIf cfg.enable {
    programs.vim = {
      enable = true;
      # Check for more plugins via:
      # nix-env -f '<nixpkgs>' -qaP -A vimPlugins
      plugins = with pkgs.vimPlugins; [
        YouCompleteMe # Must be compiled, so can't be submoduled
      ];
    };

    home.sessionVariables = {EDITOR = "vim";};
  };
}
