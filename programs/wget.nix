{
  config,
  lib,
  pkgs,
  ...
}:
with lib; let
  cfg = config.keydrain.wget;
in {
  options = {
    keydrain.wget.enable =
      mkEnableOption "Enable keydrain's wget configuration";
  };

  config = mkIf cfg.enable {home.packages = with pkgs; [wget];};
}
