{
  config,
  lib,
  pkgs,
  ...
}:
with lib; let
  cfg = config.keydrain.perl;
in {
  options = {
    keydrain.perl.enable = mkEnableOption "Enable keydrain's perl configuration";
  };

  config = mkIf cfg.enable {home.packages = with pkgs; [perl];};
}
