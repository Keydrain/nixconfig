{
  config,
  lib,
  pkgs,
  ...
}:
with lib; let
  cfg = config.keydrain.gimp;
in {
  options = {
    keydrain.gimp.enable =
      mkEnableOption "Enable keydrain's gimp configuration";
  };

  config = mkIf cfg.enable {home.packages = with pkgs; [gimp];};
}
