{
  config,
  lib,
  pkgs,
  ...
}:
with lib; let
  cfg = config.keydrain.kitty;
in {
  options = {
    keydrain.kitty.enable =
      mkEnableOption "Enable keydrain's kitty configuration";
  };

  config = mkIf cfg.enable {home.packages = with pkgs; [kitty];};
}
