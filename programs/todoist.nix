{
  config,
  lib,
  pkgs,
  ...
}:
with lib; let
  cfg = config.keydrain.todoist;
in {
  options = {
    keydrain.todoist.enable =
      mkEnableOption "Enable keydrain's todoist configuration";
  };

  config = mkIf cfg.enable {
    home.packages = with pkgs; [stable.todoist stable.todoist-electron];
  };
}
