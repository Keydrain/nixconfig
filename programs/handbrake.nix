{
  config,
  lib,
  pkgs,
  ...
}:
with lib; let
  cfg = config.keydrain.handbrake;
in {
  options = {
    keydrain.handbrake.enable =
      mkEnableOption "Enable keydrain's handbrake configuration";
  };

  config = mkIf cfg.enable {home.packages = with pkgs; [handbrake];};
}
