{
  config,
  lib,
  ...
}:
with lib; let
  cfg = config.keydrain.nix-index;
in {
  options = {
    keydrain.nix-index.enable =
      mkEnableOption "Enable keydrain's nix-index configuration";
  };

  config = mkIf cfg.enable {programs.nix-index.enable = true;};
}
