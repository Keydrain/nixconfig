{
  config,
  lib,
  pkgs,
  ...
}:
with lib; let
  cfg = config.keydrain.supertuxkart;
in {
  options = {
    keydrain.supertuxkart.enable =
      mkEnableOption "Enable keydrain's supertuxkart configuration";
  };

  config = mkIf cfg.enable {home.packages = with pkgs; [superTuxKart];};
}
