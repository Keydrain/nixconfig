{
  config,
  lib,
  pkgs,
  ...
}:
with lib; let
  cfg = config.keydrain.lutris;
in {
  options = {
    keydrain.lutris.enable =
      mkEnableOption "Enable keydrain's lutris configuration";
  };

  config = mkIf cfg.enable {
    home.packages = with pkgs; [
      wineWowPackages.waylandFull
      winetricks
      lutris
      openssl # League of Legends
      mono # League of Legends
      gnome.zenity # League of Legends
    ];
  };
}
