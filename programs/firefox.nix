{
  config,
  lib,
  ...
}:
with lib; let
  cfg = config.keydrain.firefox;
in {
  options = {
    keydrain.firefox.enable =
      mkEnableOption "Enable keydrain's firefox configuration";
    keydrain.firefox.extensions = mkOption {
      type = types.bool;
      default = true;
      description = "Enable extenions for keydrain's firefox configuration";
    };
  };

  config = mkIf cfg.enable {
    programs.firefox = {
      enable = true;
      profiles.default = {
        name = "Default";
        isDefault = true;
        extensions =
          if (cfg.extensions)
          then
            with config.nur.repos.rycee.firefox-addons; [
              clearurls
              decentraleyes
              duckduckgo-privacy-essentials
              search-by-image
              ublock-origin # Privacyguides.org
            ]
          else [];
        settings = {
          "browser.newtabpage.activity-stream.feeds.section.highlights" = true;
          "browser.newtabpage.activity-stream.showSponsored" = false;
          "browser.newtabpage.activity-stream.showSponsoredTopSites" = false;
          "browser.sessionstore.privacy_level" = 2;
          "browser.startup.page" = 3;
          "browser.urlbar.speculativeConnect.enabled" = false;
          "network.dns.disablePrefetchFromHTTPS" = true;
          "network.dns.disablePrefetch" = true;
          "network.http.referer.XOriginPolicy" = 1;
          "network.http.referer.XOriginTrimmingPolicy" = 1;
          "network.predictor.enabled" = false;
          "network.predictor.enable-prefetch" = false;
          "network.prefetch-next" = false;

          ### https://www.privacyguides.org/en/desktop-browsers/#privacy-security
          "browser.search.suggest.enabled" = false;
          "browser.urlbar.suggest.searches" = false;

          # "browser.contentblocking.category" = "strict";
          "network.cookie.cookieBehavior" = 1; #5
          "network.http.referer.disallowCrossSiteRelaxingDefault" = true;
          "network.http.referer.disallowCrossSiteRelaxingDefault.top_navigation" = true;
          "privacy.partition.network_state.ocsp_cache" = true;
          "privacy.query_stripping.enabled" = true;
          "privacy.trackingprotection.enabled" = true;
          "privacy.trackingprotection.socialtracking.enabled" = true;
          "privacy.trackingprotection.cryptomining.enabled" = true;
          "privacy.trackingprotection.fingerprinting.enabled" = true;

          "browser.urlbar.dnsResolveSingleWordsAfterSearch" = 0;
          "datareporting.policy.dataSubmissionEnabled" = false;
          "datareporting.healthreport.uploadEnabled" = false;
          "breakpad.reportURL" = "";
          "browser.tabs.crashReporting.sendReport" = false;
          "dom.security.https_only_mode" = true;
        };
      };
    };

    home.sessionVariables = {
      MOZ_DBUS_REMOTE = 1;
      MOZ_USE_XINPUT2 = 1;
    };
  };
}
