{
  config,
  lib,
  pkgs,
  ...
}:
with lib; let
  cfg = config.keydrain.vscodium;
in {
  options = {
    keydrain.vscodium.enable =
      mkEnableOption "Enable keydrain's vscodium configuration";
  };

  config = mkIf cfg.enable {home.packages = with pkgs; [vscodium];};
}
