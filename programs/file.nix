{
  config,
  lib,
  pkgs,
  ...
}:
with lib; let
  cfg = config.keydrain.file;
in {
  options = {
    keydrain.file.enable =
      mkEnableOption "Enable keydrain's file configuration";
  };

  config = mkIf cfg.enable {home.packages = with pkgs; [file];};
}
