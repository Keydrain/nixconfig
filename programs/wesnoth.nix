{
  config,
  lib,
  pkgs,
  ...
}:
with lib; let
  cfg = config.keydrain.wesnoth;
in {
  options = {
    keydrain.wesnoth.enable =
      mkEnableOption "Enable keydrain's wesnoth configuration";
  };

  config = mkIf cfg.enable {home.packages = with pkgs; [wesnoth];};
}
