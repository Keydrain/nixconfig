{
  config,
  lib,
  pkgs,
  ...
}:
with lib; let
  cfg = config.keydrain.godot;
in {
  options = {
    keydrain.godot.enable =
      mkEnableOption "Enable keydrain's godot configuration";
  };

  config = mkIf cfg.enable {home.packages = with pkgs; [godot_4];};
}
