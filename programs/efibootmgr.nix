{
  config,
  lib,
  pkgs,
  ...
}:
with lib; let
  cfg = config.keydrain.efibootmgr;
in {
  options = {
    keydrain.efibootmgr.enable =
      mkEnableOption "Enable keydrain's efibootmgr configuration";
  };

  config = mkIf cfg.enable {home.packages = with pkgs; [efibootmgr];};
}
