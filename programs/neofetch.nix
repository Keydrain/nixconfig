{
  config,
  lib,
  pkgs,
  ...
}:
with lib; let
  cfg = config.keydrain.neofetch;
in {
  options = {
    keydrain.neofetch.enable =
      mkEnableOption "Enable keydrain's neofetch configuration";
  };

  config = mkIf cfg.enable {home.packages = with pkgs; [neofetch];};
}
