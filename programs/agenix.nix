{
  config,
  lib,
  pkgs,
  ...
}:
with lib; let
  cfg = config.keydrain.agenix;
in {
  options = {
    keydrain.agenix.enable =
      mkEnableOption "Enable keydrain's agenix configuration";
  };

  config = mkIf cfg.enable {home.packages = with pkgs; [agenix];};
}
