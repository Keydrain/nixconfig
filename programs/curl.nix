{
  config,
  lib,
  pkgs,
  ...
}:
with lib; let
  cfg = config.keydrain.curl;
in {
  options = {
    keydrain.curl.enable =
      mkEnableOption "Enable keydrain's curl configuration";
  };

  config = mkIf cfg.enable {home.packages = with pkgs; [curlFull];};
}
