{
  config,
  lib,
  pkgs,
  ...
}:
with lib; let
  cfg = config.keydrain.audacity;
in {
  options = {
    keydrain.audacity.enable =
      mkEnableOption "Enable keydrain's audacity configuration";
  };

  config = mkIf cfg.enable {home.packages = with pkgs; [audacity];};
}
