{
  config,
  lib,
  pkgs,
  ...
}:
with lib; let
  cfg = config.keydrain.git;
in {
  options = {
    keydrain.git.enable = mkEnableOption "Enable keydrain's git configuration";
  };

  config = mkIf cfg.enable {
    home.packages = with pkgs; [
      git
      git-lfs
      git-crypt
      gitAndTools.diff-so-fancy
    ];

    programs = {
      zsh.shellAliases =
        if (config.programs.zsh.enable)
        then {g = "git";}
        else {};
    };
  };
}
