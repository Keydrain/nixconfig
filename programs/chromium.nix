{
  config,
  lib,
  ...
}:
with lib; let
  cfg = config.keydrain.chromium;
in {
  options = {
    keydrain.chromium.enable =
      mkEnableOption "Enable keydrain's chromium configuration";
  };

  config = mkIf cfg.enable {
    programs.chromium = {
      enable = true;
      extensions = [
        {id = "cmedhionkhpnakcndndgjdbohmhepckk";} # adblock for youtube
        {id = "caclkomlalccbpcdllchkeecicepbmbm";} # advanced font settings
        {id = "ghbmnnjooekpmoecnnnilnnbdlolhkhi";} # google docs offline
        {id = "bmnlcjabgnpnenekpadlanbbkooimhnj";} # honey
        {id = "gcbommkclmclpchllfjekcdonpmejbdp";} # https everywhere
        {id = "gojamcfopckidlocpkbelmpjcgmbgjcl";} # no coin
        {id = "kbmfpngjjgdllneeigpgjifpgocmfgmb";} # reddit enhancement suite
        {id = "cjpalhdlnbpafiamejdnhcphjbkeiagm";} # ublock origin
      ];
    };
  };
}
