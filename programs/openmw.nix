{
  config,
  lib,
  pkgs,
  ...
}:
with lib; let
  cfg = config.keydrain.openmw;
in {
  options = {
    keydrain.openmw.enable =
      mkEnableOption "Enable keydrain's openmw configuration";
    keydrain.openmw.multiplayer = mkOption {
      type = types.bool;
      default = false;
      description = "Enable multiplayer for keydrain's openmw configuration";
    };
  };

  config = mkIf cfg.enable {
    home.packages = with pkgs;
      if (cfg.multiplayer)
      then [
        openmw-tes3mp
        portmod
      ]
      else [
        openmw
        portmod
      ];
  };
}
