{
  config,
  lib,
  pkgs,
  ...
}:
with lib; let
  cfg = config.keydrain.zip;
in {
  options = {
    keydrain.zip.enable = mkEnableOption "Enable keydrain's zip configuration";
  };

  config = mkIf cfg.enable {home.packages = with pkgs; [zip unzip];};
}
