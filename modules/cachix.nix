{
  config,
  lib,
  pkgs,
  ...
}:
with lib; let
  cfg = config.keydrain.cachix;
in {
  options = {
    keydrain.cachix.enable =
      mkEnableOption "Enable keydrain's cachix configuration";
  };

  config = mkIf cfg.enable {
    environment.systemPackages = with pkgs; [cachix];

    nix.settings = {
      substituters = [
        "https://cache.nixos.org"
        "https://nix-community.cachix.org"
        "https://arm.cachix.org"
      ];
      trusted-public-keys = [
        "nix-community.cachix.org-1:mB9FSh9qf2dCimDSUo8Zy7bkq5CX+/rkCWyvRCYg3Fs="
        "arm.cachix.org-1:5BZ2kjoL1q6nWhlnrbAl+G7ThY7+HaBRD9PZzqZkbnM="
      ];
    };
  };
}
