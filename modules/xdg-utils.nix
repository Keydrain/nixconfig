{
  config,
  lib,
  pkgs,
  ...
}:
with lib; let
  cfg = config.keydrain.xdg-utils;
in {
  options = {
    keydrain.xdg-utils.enable =
      mkEnableOption "Enable keydrain's xdg-utils configuration";
  };

  config = mkIf cfg.enable {
    environment.systemPackages = with pkgs; [xdg-utils];

    # xdg.portal = {
    #   enable = true;
    #   gtkUsePortal = true;
    # };

    # Ensure localized scripts have a destination
    system.activationScripts = {
      xdg-open-link.text = ''
        ln -sfn ${pkgs.xdg-utils}/bin/xdg-open /usr/bin/xdg-open
      '';
    };
  };
}
