{
  config,
  lib,
  variables,
  ...
}:
with lib; let
  cfg = config.keydrain.docker;
in {
  options = {
    keydrain.docker.enable =
      mkEnableOption "Enable keydrain's docker configuration";
    # keydrain.docker.useDHCP = mkOption {
    #   type = types.nullOr types.bool;
    #   default = null;
    #   description = "Enable dhcp on keydrain's docker configuration";
    # };
  };

  config = mkIf cfg.enable {
    # networking.interfaces.docker0.useDHCP = cfg.useDHCP;
    virtualisation.docker = {
      enable = true;
      autoPrune = {
        enable = true;
        dates = "weekly";
      };
      extraOptions = "--bip=172.17.0.1/16";
      daemon.settings = {exec-opts = ["native.cgroupdriver=systemd"];};
    };

    users.users.${variables.primaryUser} = {extraGroups = ["docker"];};
  };
}
