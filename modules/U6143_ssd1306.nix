{
  config,
  lib,
  pkgs,
  ...
}:
with lib; let
  cfg = config.keydrain.U6143_ssd1306;
in {
  options = {
    keydrain.U6143_ssd1306.enable =
      mkEnableOption "Enable keydrain's U6143_ssd1306 configuration";
  };

  config = mkIf cfg.enable {
    systemd.services.U6143_ssd1306 = {
      unitConfig = {
        After = "network-online.target";
        Requisite = "network-online.target";
      };
      serviceConfig = {
        Type = "simple";
        RemainAfterExit = "yes";
      };
      path = with pkgs; [gawk procps]; # Needs awk and top
      script = let
        display = "${pkgs.U6143_ssd1306}/bin/display";
      in ''
        ${display}
      '';
      wantedBy = ["multi-user.target"];
    };
  };
}
