{
  config,
  lib,
  ...
}:
with lib; let
  cfg = config.keydrain.lxd;
in {
  options = {
    keydrain.lxd.enable = mkEnableOption "Enable keydrain's lxd configuration";
  };

  config = mkIf cfg.enable {
    virtualisation.lxd.enable = true;
    virtualisation.lxc.lxcfs.enable = true;
    virtualisation.lxd.recommendedSysctlSettings = true;
  };
}
