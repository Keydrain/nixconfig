{
  config,
  lib,
  variables,
  ...
}:
with lib; let
  cfg = config.keydrain.openssh;
in {
  options = {
    keydrain.openssh.enable =
      mkEnableOption "Enable keydrain's openssh configuration";
    keydrain.openssh.passwordAuth = mkOption {
      type = types.bool;
      default = false;
      description = "Enable passwords for keydrain's openssh configuration";
    };
  };

  config = mkIf cfg.enable {
    users.users.${variables.primaryUser}.openssh.authorizedKeys.keys = [
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIJPuobAG/INsDM2ZQ5pBg5DculhV0xLmrrrPNDQzgZmJ clint@10.10.0.1"
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIKGb4b5OUiVzFmovxrDhY3ifhdvLzWhZvHmT12HR28Ma dylan@10.10.0.2"
    ];
    services.openssh = {
      enable = true;
      settings = {
        PasswordAuthentication = cfg.passwordAuth;
        KbdInteractiveAuthentication = false;
      };
      allowSFTP = false;
      knownHosts = {
        Frampt = {
          publicKey = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIJPuobAG/INsDM2ZQ5pBg5DculhV0xLmrrrPNDQzgZmJ";
        };
        BiggerChungus = {
          publicKey = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIKGb4b5OUiVzFmovxrDhY3ifhdvLzWhZvHmT12HR28Ma";
        };
      };
      extraConfig = ''
        AllowTcpForwarding yes
        X11Forwarding no
        AllowAgentForwarding no
        AllowStreamLocalForwarding no
      '';
    };
  };
}
