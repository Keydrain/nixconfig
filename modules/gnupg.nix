{
  config,
  lib,
  pkgs,
  ...
}:
with lib; let
  cfg = config.keydrain.gnupg;
in {
  options = {
    keydrain.gnupg.enable =
      mkEnableOption "Enable keydrain's gnupg configuration";
  };

  config = mkIf cfg.enable {
    environment.systemPackages = with pkgs;
      if (config.services.xserver.desktopManager.gnome.enable)
      then [
        pinentry-gnome
        ssh-to-pgp
      ]
      else [
        pinentry
        ssh-to-pgp
      ];

    programs.gnupg.agent =
      if (config.services.xserver.desktopManager.gnome.enable)
      then {
        enable = true;
        enableSSHSupport = true;
        pinentryPackage = "gnome3";
      }
      else {
        enable = true;
        enableSSHSupport = true;
        pinentryPackage = "curses";
      };
  };
}
