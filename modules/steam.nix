{
  config,
  lib,
  pkgs,
  ...
}:
with lib; let
  cfg = config.keydrain.steam;
in {
  options = {
    keydrain.steam = {
      enable = mkEnableOption "Enable keydrain's steam configuration";
    };
  };

  config = mkIf cfg.enable {
    environment.systemPackages = with pkgs; [
      vulkan-tools
      master.steam
      master.steam.run
      unstable.steamcmd
      unstable.protontricks
      unstable.r2mod_cli
      # master.linuxKernel.packages.linux_6_3.xone
    ];

    # Steam
    programs.steam = {
      enable = true;
      remotePlay.openFirewall = true;
      dedicatedServer.openFirewall = false;
    };

    # Gamemode
    programs.gamemode = {
      enable = true;
      settings = {
        general = {renice = 10;};
        custom = {
          start = "${pkgs.libnotify}/bin/notify-send 'GameMode' 'GameMode has started' --icon=applications-games";
          end = "${pkgs.libnotify}/bin/notify-send 'GameMode' 'GameMode has ended' --icon=applications-games";
        };
      };
    };

    # Controllers
    hardware.steam-hardware.enable = true;
    hardware.xone.enable = true;
    hardware.xpadneo.enable = true;

    # 32-bit libs
    hardware.pulseaudio.support32Bit = true;
    hardware.opengl.driSupport32Bit = true;
    services.pipewire.alsa.support32Bit = config.services.pipewire.alsa.enable;
    services.jack.alsa.support32Bit = config.services.jack.alsa.enable;
  };
}
