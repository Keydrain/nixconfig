{
  config,
  lib,
  pkgs,
  variables,
  ...
}:
with lib; let
  cfg = config.keydrain.fprintd;
in {
  options = {
    keydrain.fprintd.enable =
      mkEnableOption "Enable keydrain's fprintd configuration";
  };

  config = mkIf cfg.enable {
    environment.systemPackages = with pkgs; [stable.fprintd stable.libfprint];

    services.fprintd.enable = true;
    security.pam.services.login.fprintAuth = true;
    security.pam.services.xscreensaver.fprintAuth = true;

    users.users.${variables.primaryUser} = {extraGroups = ["input"];};
  };
}
