{
  config,
  lib,
  pkgs,
  variables,
  ...
}:
with lib; let
  cfg = config.keydrain.qFlipper;
in {
  options = {
    keydrain.qFlipper.enable =
      mkEnableOption "Enable keydrain's qFlipper configuration";
  };

  config = mkIf cfg.enable {
    environment.systemPackages = with pkgs; [qFlipper];
    users.users.${variables.primaryUser} = {extraGroups = ["dialout"];};
    # These extra rules are taken from: https://github.com/flipperdevices/qFlipper/blob/dev/installer-assets/udev/42-flipperzero.rules
    services.udev.extraRules = ''
      #Flipper Zero serial port
      SUBSYSTEMS=="usb", ATTRS{idVendor}=="0483", ATTRS{idProduct}=="5740", ATTRS{manufacturer}=="Flipper Devices Inc.", TAG+="uaccess", GROUP="dialout"
      #Flipper Zero DFU
      SUBSYSTEMS=="usb", ATTRS{idVendor}=="0483", ATTRS{idProduct}=="df11", ATTRS{manufacturer}=="STMicroelectronics", TAG+="uaccess", GROUP="dialout"
      #Flipper ESP32s2 BlackMagic
      SUBSYSTEMS=="usb", ATTRS{idVendor}=="303a", ATTRS{idProduct}=="40??", ATTRS{manufacturer}=="Flipper Devices Inc.", TAG+="uaccess", GROUP="dialout"
    '';
  };
}
