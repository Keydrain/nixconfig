{
  config,
  lib,
  variables,
  ...
}:
# Full refresh:
# rm -rf /etc/rancher /var/lib/rancher
# kubectl delete node <node name>
# TODO: k3s nodes should have tmp file systems
with lib; let
  cfg = config.keydrain.k3s;
in {
  options = {
    keydrain.k3s.enable = mkEnableOption "Enable keydrain's k3s configuration";
    keydrain.k3s.role = mkOption {
      type = types.str;
      default = "agent";
      description = "What role should be used for keydrain's k3s configuration";
    };
    keydrain.k3s.labels = mkOption {
      type = types.str;
      default = "";
      description = "What extra labels should be added for keydrain's k3s configuration";
    };
  };

  config = mkIf cfg.enable {
    networking.firewall = {
      allowedTCPPorts = [
        6443 # API Server
        10250 # Kubelet Metrics
        2379 # HA with embeded etcd
        2380 # HA with embeded etcd
        # 5473 # Calico Typha
        7946 # MetalLB
      ];
      allowedUDPPorts = [
        8472 # Flannel VXLAN
        # 4789 # Calico (Flannel) VXLAN
        7946 # MetalLB
      ];
    };

    age.secrets.k3s-server-token = {file = ../secrets/k3s-server-token.age;};

    services.k3s = {
      enable = true;
      role = cfg.role;
      serverAddr = "https://172.24.0.2:6443";
      tokenFile = config.age.secrets.k3s-server-token.path;
      extraFlags = toString ((
          if (cfg.role == "server")
          then [
            # "--cluster-init" # Needed only on first node startup (disable serverAddr above as well)
            "--write-kubeconfig /home/${variables.primaryUser}/.config/k3s.yaml"
            "--write-kubeconfig-mode 644"
            "--tls-san 172.24.0.2" # Abyss HAProxy
            "--cluster-cidr 172.30.0.0/16"
            "--service-cidr 172.31.0.0/16"
            "--cluster-dns 172.31.0.10"
            # "--flannel-backend none" # Needed by Calico
            # "--disable-network-policy" # Needed by Calico
            "--disable servicelb" # Needed by MetalLB
            "--disable traefik" # Needed by MetalLB
            "--disable-cloud-controller" # Self hosted cluster
          ]
          else []
        )
        ++ (
          if (cfg.labels != "")
          then ["--node-label" cfg.labels]
          else []
        )
        ++ ["--data-dir /var/lib/rancher/k3s"]);
    };

    # K8s secrets that are referenced by deployments but should be encrypted at
    # rest by agenix.
    age.secrets.k3s-gitlab-runner-amd64-secret = {
      file = ../secrets/k3s-gitlab-runner-amd64-secret.age;
      path = "/var/lib/rancher/k3s/server/manifests/gitlab-runner-amd64-secret.yaml";
      mode = "0600";
      owner = "root";
      group = "root";
    };

    age.secrets.k3s-gitlab-runner-arm64-secret = {
      file = ../secrets/k3s-gitlab-runner-arm64-secret.age;
      path = "/var/lib/rancher/k3s/server/manifests/gitlab-runner-arm64-secret.yaml";
      mode = "0600";
      owner = "root";
      group = "root";
    };

    age.secrets.k3s-gitlab-registry-secret = {
      file = ../secrets/k3s-gitlab-registry-secret.age;
      path = "/var/lib/rancher/k3s/server/manifests/gitlab-registry-secret.yaml";
      mode = "0600";
      owner = "root";
      group = "root";
    };

    age.secrets.k3s-traefik-dashboard-auth-secret = {
      file = ../secrets/k3s-traefik-dashboard-auth-secret.age;
      path = "/var/lib/rancher/k3s/server/manifests/traefik-dashboard-auth-secret.yaml";
      mode = "0600";
      owner = "root";
      group = "root";
    };

    age.secrets.k3s-cert-manager-cloudflare-secret = {
      file = ../secrets/k3s-cert-manager-cloudflare-secret.age;
      path = "/var/lib/rancher/k3s/server/manifests/cert-manager-cloudflare-secret.yaml";
      mode = "0600";
      owner = "root";
      group = "root";
    };

    age.secrets.k3s-ddclient-cloudflare-config-secret = {
      file = ../secrets/k3s-ddclient-cloudflare-config-secret.age;
      path = "/var/lib/rancher/k3s/server/manifests/ddclient-cloudflare-config-secret.yaml";
      mode = "0600";
      owner = "root";
      group = "root";
    };

    age.secrets.k3s-foundry-vtt-secret = {
      file = ../secrets/k3s-foundry-vtt-secret.age;
      path = "/var/lib/rancher/k3s/server/manifests/foundry-vtt-secret.yaml";
      mode = "0600";
      owner = "root";
      group = "root";
    };
  };
}
