{
  config,
  lib,
  pkgs,
  variables,
  ...
}:
with lib; let
  cfg = config.keydrain.kubectl;
in {
  options = {
    keydrain.kubectl.enable =
      mkEnableOption "Enable keydrain's kubectl configuration";
  };

  config = mkIf cfg.enable {
    environment.systemPackages = with pkgs; [
      kubectl
      krew
      stern
      kubernetes-helm
      helmfile
      kustomize
      k9s
    ];

    age.secrets.kubectl-config = {
      file = ../secrets/kubectl-config.age;
      path = "/home/${variables.primaryUser}/.kube/config";
      mode = "0600";
      owner = "${variables.primaryUser}";
      group = "users";
    };

    # TODO: Krew Setup
    # krew install krew
    # PLUGINS=("ctx" "grep" "krew" "match-name" "ns" "relay")
    # for plugin in "${PLUGINS[@]}"; do
    #   kubectl krew install $plugin
    # done

    # TODO: helm plugin setup
    # helm plugin install https://github.com/databus23/helm-diff
  };
}
