{
  config,
  lib,
  variables,
  ...
}:
with lib; let
  cfg = config.keydrain.sysstat;
in {
  options = {
    keydrain.sysstat.enable =
      mkEnableOption "Enable keydrain's sysstat configuration";
  };

  config = mkIf cfg.enable {
    services.sysstat = {
      enable = true;
      collect-frequency = "*:00/15";
    };
  };
}
