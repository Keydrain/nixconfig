{
  config,
  lib,
  ...
}:
with lib; let
  cfg = config.keydrain.fwupd;
in {
  options = {
    keydrain.fwupd.enable =
      mkEnableOption "Enable keydrain's fwupd configuration";
  };

  config = mkIf cfg.enable {services.fwupd.enable = true;};
}
