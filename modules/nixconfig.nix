{
  config,
  lib,
  pkgs,
  variables,
  ...
}:
with lib; let
  cfg = config.keydrain.nixconfig;
in {
  options = {
    keydrain.nixconfig.enable =
      mkEnableOption "Enable keydrain's nixconfig configuration";
    keydrain.nixconfig.notifications = mkOption {
      type = types.bool;
      default = false;
      description = "Enable notifications for keydrain's nixconfig configuration";
    };
    keydrain.nixconfig.timers.enable = mkOption {
      type = types.bool;
      default = true;
      description = "Enable timers for keydrain's nixconfig configuration";
    };
    keydrain.nixconfig.timers.calendar = mkOption {
      type = types.str;
      default = "daily";
      description = "Configure the calendar frequency for timers for keydrain's nixconfig configuration";
    };
    keydrain.nixconfig.timers.delaySec = mkOption {
      type = types.str;
      default = "86400";
      description = "Configure the calendar delay for timers for keydrain's nixconfig configuration";
    };
  };

  config = mkIf cfg.enable {
    systemd.timers.nixconfig = mkIf cfg.timers.enable {
      timerConfig = {
        OnCalendar = cfg.timers.calendar;
        Persistent = true;
        RandomizedDelaySec = cfg.timers.delaySec;
      };
      wantedBy = ["multi-user.target"];
    };

    systemd.services.nixconfig-setup = {
      unitConfig = {
        After = "network-online.target";
        Requisite = "network-online.target";
      };
      serviceConfig = {Type = "idle";};
      restartIfChanged = false;
      path = with pkgs; [coreutils git openssh];
      environment = {
        SHELL = "/run/current-system/sw/bin/bash";
        HOME = "/root";
      };
      script = ''
        mkdir -p /home/${variables.primaryUser}/git
        if [[ ! -d /home/${variables.primaryUser}/git/nixconfig ]]; then
          git config --global http.sslVerify "false"
          git clone https://gitlab.com/keydrain/nixconfig.git /home/${variables.primaryUser}/git/nixconfig
          git config --global http.sslVerify "true"
          git --git-dir=/home/${variables.primaryUser}/git/nixconfig/.git --work-tree=. -C /home/${variables.primaryUser}/git/nixconfig remote set-url --push origin git@gitlab.com:Keydrain/nixconfig.git
          chown -R ${variables.primaryUser}:users /home/${variables.primaryUser}/git
          rm -rf /etc/nixos
          ln -s /home/${variables.primaryUser}/git/nixconfig /etc/nixos
        fi
      '';
      wantedBy = ["multi-user.target"];
    };

    systemd.services.nixconfig = {
      unitConfig = {
        After = "network-online.target";
        Requisite = "network-online.target";
        OnFailure =
          if (cfg.notifications)
          then "notify@failure-nixconfig-System\\x20Configuration.service"
          else "";
        OnSuccess =
          if (cfg.notifications)
          then "notify@success-nixconfig-System\\x20Configuration.service"
          else "";
        Wants =
          if (cfg.notifications)
          then "notify@start-nixconfig-System\\x20Configuration.service"
          else "";
      };
      serviceConfig = {
        User = variables.primaryUser;
        Type = "oneshot";
      };
      restartIfChanged = false;
      path = with pkgs; [coreutils git openssh gnugrep gawk];
      environment = {SHELL = "/run/current-system/sw/bin/bash";};
      preStart =
        if (cfg.notifications)
        then ''
          session=$(loginctl list-sessions | grep $USER | awk '{print $1}')
          until [[ "$(loginctl show-session -p State $session)" = "State=active" ]]; do sleep 60; done
          until [[ "$(loginctl show-session -p LockedHint $session)" = "LockedHint=no" ]]; do sleep 60; done
        ''
        else "";
      script = ''
        if git --git-dir=/home/$USER/git/nixconfig/.git --work-tree=. -C /home/$USER/git/nixconfig diff-index --quiet HEAD --; then
          sleep 0.5
          git --git-dir=/home/$USER/git/nixconfig/.git --work-tree=. -C /home/$USER/git/nixconfig pull origin main
        fi
      '';
      wantedBy = [];
    };
  };
}
