{
  config,
  lib,
  variables,
  ...
}:
with lib; let
  cfg = config.keydrain.zerotier;
in {
  options = {
    keydrain.zerotier.enable =
      mkEnableOption "Enable keydrain's zerotier configuration";
  };

  config = mkIf cfg.enable {
    services.zerotierone = {
      enable = true;
      joinNetworks = ["9bee8941b5cbc9f9"];
    };
  };
}
