{
  config,
  lib,
  pkgs,
  ...
}:
with lib; let
  cfg = config.keydrain.gnome;
in {
  options = {
    keydrain.gnome.enable =
      mkEnableOption "Enable keydrain's gnome configuration";
  };

  config = mkIf cfg.enable {
    environment.systemPackages = with pkgs; [
      gnome.gnome-shell-extensions
      gnome3.gnome-tweaks
      gnomeExtensions.appindicator
      gnomeExtensions.vitals
      orchis
      kora-icon-theme
      tela-icon-theme
      papirus-icon-theme
      paper-icon-theme
      flat-remix-icon-theme
    ];

    # Exclude some gnome packages
    environment.gnome.excludePackages = with pkgs; [
      gnome.cheese
      gnome.gnome-music
      gnome.totem
      gnome.epiphany
      gnome.geary
      gnome.gnome-disk-utility
      gnome.gnome-packagekit
    ];

    # Disable packagekit
    systemd.services.packagekit.enable = false;

    # Gnome Settings
    services.xserver.desktopManager.gnome.enable = true;
    services.displayManager.defaultSession = "gnome";
    services.libinput.enable = true;
    programs.xwayland.enable = true;
    programs.dconf.enable = true;
    services.udev.packages = with pkgs; [gnome3.gnome-settings-daemon];

    # Wayland env settings for QT apps
    environment.sessionVariables = {"QT_QPA_PLATFORM" = "wayland";};
  };
}
