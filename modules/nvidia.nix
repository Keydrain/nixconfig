{
  config,
  lib,
  pkgs,
  ...
}:
with lib; let
  cfg = config.keydrain.nvidia;
in {
  options = {
    keydrain.nvidia.enable =
      mkEnableOption "Enable keydrain's nvidia configuration";
  };

  config = mkIf cfg.enable {
    environment.systemPackages = with pkgs; [glxinfo];

    # Driver services for Nvidia GPU
    hardware = {
      opengl.enable = true;
      nvidia = {
        modesetting.enable = true;
        prime.offload.enable = false;
      };
    };
    services.xserver.videoDrivers = ["nvidia"];
  };
}
