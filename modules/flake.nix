{
  config,
  lib,
  pkgs,
  ...
}:
with lib; let
  cfg = config.keydrain.flake;
in {
  options = {
    keydrain.flake.enable =
      mkEnableOption "Enable keydrain's flake configuration";
  };

  config = mkIf cfg.enable {
    nix = {
      package = pkgs.nixFlakes;
      extraOptions = ''
        experimental-features = nix-command flakes
      '';
    };
  };
}
