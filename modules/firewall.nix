{
  config,
  lib,
  ...
}:
with lib; let
  cfg = config.keydrain.firewall;
in {
  options = {
    keydrain.firewall.enable =
      mkEnableOption "Enable keydrain's firewall configuration";
  };

  config = mkIf cfg.enable {networking.firewall.enable = true;};
}
