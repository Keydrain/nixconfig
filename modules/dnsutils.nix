{
  config,
  lib,
  pkgs,
  ...
}:
with lib; let
  cfg = config.keydrain.dnsutils;
in {
  options = {
    keydrain.dnsutils.enable =
      mkEnableOption "Enable keydrain's dnsutils configuration";
  };

  config =
    mkIf cfg.enable {environment.systemPackages = with pkgs; [dnsutils];};
}
