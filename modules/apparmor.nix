{
  config,
  lib,
  pkgs,
  ...
}:
with lib; let
  cfg = config.keydrain.apparmor;
in {
  options = {
    keydrain.apparmor.enable =
      mkEnableOption "Enable keydrain's apparmor configuration";
  };

  config = mkIf cfg.enable {
    environment.systemPackages = with pkgs; [apparmor-profiles];
    security.apparmor.enable = true;
  };
}
