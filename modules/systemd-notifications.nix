{
  pkgs,
  variables,
  ...
}: {
  options = {};

  config = {
    systemd.services = {
      "notify@" = {
        serviceConfig.User = variables.primaryUser;
        environment = {
          INPUT = "%i";
          SHELL = "/run/current-system/sw/bin/bash";
        };
        path = with pkgs; [coreutils libnotify procps gnugrep];
        script = ''
          export $(cat /proc/$(pgrep "gnome-session" -u "$USER" | head -n1)/environ | grep -z '^DBUS_SESSION_BUS_ADDRESS=')
          export type=''${INPUT%%-*}
          export name=$(systemd-escape -u ''${INPUT##*-})
          export service=''${INPUT%-*}
          export service=''${service#*-}
          case ''${type} in
            failure)
              notify-send "''${name}" "''${service} has failed. Run journalctl -u ''${service} for details." --icon=face-sick
              ;;
            success)
              notify-send "''${name}" "''${service} has completed." --icon=face-smile-big
              ;;
            start)
              notify-send "''${name}" "''${service} has begun." --icon=system-run
              ;;
            *)
              notify-send "''${name}" "''${service} is of type ''${type}." --icon=face-confused
              ;;
          esac
        '';
      };
    };
  };
}
