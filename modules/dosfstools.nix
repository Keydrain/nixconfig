{
  config,
  lib,
  pkgs,
  ...
}:
with lib; let
  cfg = config.keydrain.dosfstools;
in {
  options = {
    keydrain.dosfstools.enable =
      mkEnableOption "Enable keydrain's dosfstools configuration";
  };

  config =
    mkIf cfg.enable {environment.systemPackages = with pkgs; [dosfstools];};
}
