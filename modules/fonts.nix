{
  config,
  lib,
  pkgs,
  ...
}:
with lib; let
  cfg = config.keydrain.fonts;
in {
  options = {
    keydrain.fonts.enable =
      mkEnableOption "Enable keydrain's fonts configuration";
  };

  config = mkIf cfg.enable {
    console.font = "Lat2-Terminus16";
    fonts = {
      fontDir.enable = true;
      fontconfig.useEmbeddedBitmaps = true;
      packages = with pkgs; [
        dejavu_fonts
        font-awesome
        nerdfonts
        powerline-fonts
      ];
    };
  };
}
