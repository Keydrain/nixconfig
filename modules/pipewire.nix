{
  config,
  lib,
  pkgs,
  variables,
  ...
}:
with lib; let
  cfg = config.keydrain.pipewire;
in {
  options = {
    keydrain.pipewire.enable =
      mkEnableOption "Enable keydrain's pipewire configuration";
  };

  config = mkIf cfg.enable {
    environment.systemPackages = with pkgs; [helvum];

    sound.enable = false;
    hardware.pulseaudio.enable = false;
    services.jack.alsa.enable = false;

    services.pipewire = {
      enable = true;
      pulse.enable = true;
      # config.pipewire-pulse = {}; # https://gitlab.freedesktop.org/pipewire/pipewire/-/blob/0.3.40/src/daemon/pipewire-pulse.conf.in
      jack.enable = true;
      alsa.enable = true;
    };

    security.wrappers.noisetorch = {
      source = "${pkgs.noisetorch.out}/bin/noisetorch";
      owner = variables.primaryUser;
      group = "users";
      capabilities = "CAP_SYS_RESOURCE=+ep";
    };

    users.users.${variables.primaryUser} = {extraGroups = ["audio"];};
  };
}
