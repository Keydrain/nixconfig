{
  config,
  lib,
  pkgs,
  variables,
  ...
}:
with lib; let
  cfg = config.keydrain.borgbackup;
  username = variables.primaryUser;
  hostname = config.networking.hostName;
  excludeFile = pkgs.writeText "excludefile" ''
    /home/lost+found
    /home/swapfile
    /home/*/.local/share/Steam
    /home/*/.local/etc/libvirt/*.qcow2
    /home/*/.cache
  '';
  drive-path = "/run/media/${username}/${cfg.driveName}";
in {
  options = {
    keydrain.borgbackup.enable =
      mkEnableOption "Enable keydrain's borgbackup configuration";
    keydrain.borgbackup.notifications = mkOption {
      type = types.bool;
      default = false;
      description = "Enable notifications for keydrain's borgbackup configuration";
    };
    keydrain.borgbackup.driveName = mkOption {
      type = types.str;
      default = "borg-backup";
      description = "Name of the drive for keydrain's borgbackup configuration";
    };
  };

  config = mkIf cfg.enable {
    environment.systemPackages = with pkgs; [borgbackup];

    systemd.services.borg-backup = {
      unitConfig = {
        After = "run-media-${username}-borg\\x2dbackup.mount";
        Requires = "run-media-${username}-borg\\x2dbackup.mount";
        OnFailure =
          if (cfg.notifications)
          then "notify@failure-borg-backup-Borg\\x20Backup.service"
          else "";
        OnSuccess =
          if (cfg.notifications)
          then "notify@success-borg-backup-Borg\\x20Backup.service"
          else "";
        Wants =
          if (cfg.notifications)
          then "notify@start-borg-backup-Borg\\x20Backup.service"
          else "";
      };
      serviceConfig = {
        Type = "oneshot";
        CPUSchedulingPolicy = "idle";
        IOSchedulingClass = "idle";
        PrivateTmp = true;
        ProtectSystem = "strict";
        ReadWritePaths = ["/root/.config/borg" "/root/.cache/borg" "${drive-path}"];
        User = "root";
        Group = "root";
      };
      environment = {
        BORG_REPO = "${drive-path}";
        SHELL = "/run/current-system/sw/bin/bash";
      };
      path = with pkgs; [coreutils borgbackup gnugrep gawk];
      preStart =
        if (cfg.notifications)
        then ''
          session=$(loginctl list-sessions | grep ${username} | awk '{print $1}')
          until [[ "$(loginctl show-session -p State $session)" = "State=active" ]]; do sleep 60; done
          until [[ "$(loginctl show-session -p LockedHint $session)" = "LockedHint=no" ]]; do sleep 60; done
        ''
        else "";
      script = ''
        archiveName="${hostname}-$(date +%Y-%m-%dT%H:%M:%S)"
        archiveSuffix=".failed"

        borg create --stats --compression auto,zstd --exclude-from ${excludeFile} --exclude-caches "::$archiveName$archiveSuffix" /home
        borg rename "::$archiveName$archiveSuffix" "$archiveName"

        borg prune --keep-monthly=-1 --keep-within=1y --prefix "${hostname}-"

        # Cleanup lock files
        sleep 30
      '';
      wantedBy = ["run-media-${username}-borg\\x2dbackup.mount"];
    };
  };
}
