{
  config,
  lib,
  pkgs,
  ...
}:
with lib; let
  cfg = config.keydrain.anbox;
in {
  options = {
    keydrain.anbox.enable =
      mkEnableOption "Enable keydrain's anbox configuration";
  };

  config = mkIf cfg.enable {
    environment.systemPackages = with pkgs; [anbox waydroid];

    boot.kernelPackages = pkgs.linuxKernel.packages.linux_xanmod; # Waydroid
  };
}
