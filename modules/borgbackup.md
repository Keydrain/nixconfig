# Borg Backup Setup Steps

## Setup the backup volume

This module expects a removable drive to be named `borg-backup` and expects it to be mounted to `/run/media/${whoami}/borg-backup`.

### Init the repo
```
borg init --encryption none /run/media/username/borg-backup
```

### Create the first backup
```
sudo su
BORG_REPO=/run/media/username/borg-backup; borg create --compression auto,zstd --exclude '/home/lost+found' --exclude '/home/*/.local/share/Steam' --exclude '/home/*/.local/etc/libvirt/*.qcow2*' --exclude-caches ::$(hostname)-$(date +%Y-%m-%dT%H:%M:%S) /home --stats --progress
```

This backup may take a long time to run, depending on the amount of data in `/home`.

## Confirm

Remove the drive after the initial backup has succeeded and re-insert to confirm proper trigger and completion.
