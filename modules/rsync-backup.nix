{
  config,
  lib,
  pkgs,
  variables,
  ...
}:
with lib; let
  cfg = config.keydrain.rsync-backup;
in {
  options = {
    keydrain.rsync-backup.enable =
      mkEnableOption "Enable keydrain's rsync backup configuration";
    keydrain.rsync-backup.notifications = mkOption {
      type = types.bool;
      default = false;
      description = "Enable notifications for keydrain's rsync backup configuration";
    };
    keydrain.rsync-backup.timers.enable = mkOption {
      type = types.bool;
      default = true;
      description = "Enable timers for keydrain's rsync backup configuration";
    };
    keydrain.rsync-backup.timers.calendar = mkOption {
      type = types.str;
      default = "daily";
      description = "Configure the calendar frequency for timers for keydrain's rsync backup configuration";
    };
    keydrain.rsync-backup.timers.delaySec = mkOption {
      type = types.str;
      default = "86400";
      description = "Configure the calendar delay for timers for keydrain's rsync backup configuration";
    };
  };

  config = mkIf cfg.enable {
    environment.systemPackages = with pkgs; [rsync];

    systemd.timers.rsync-backup = mkIf cfg.timers.enable {
      timerConfig = {
        OnCalendar = cfg.timers.calendar;
        Persistent = true;
        RandomizedDelaySec = cfg.timers.delaySec;
      };
      wantedBy = ["multi-user.target"];
    };

    systemd.services.rsync-backup = {
      unitConfig = {
        After = "nfs-abyss-${variables.primaryUser}.mount";
        # Requisite = "nfs-abyss-${variables.primaryUser}.mount";
        Requires = "nfs-abyss-${variables.primaryUser}.mount";
        OnFailure =
          if (cfg.notifications)
          then "notify@failure-rsync-backup-RSync\\x20Backup.service"
          else "";
        OnSuccess =
          if (cfg.notifications)
          then "notify@success-rsync-backup-RSync\\x20Backup.service"
          else "";
        Wants =
          if (cfg.notifications)
          then "notify@start-rsync-backup-RSync\\x20Backup.service"
          else "";
      };
      serviceConfig = {
        User = variables.primaryUser;
        Type = "oneshot";
      };
      environment = {SHELL = "/run/current-system/sw/bin/bash";};
      path = with pkgs; [coreutils rsync gnugrep gawk];
      preStart =
        if (cfg.notifications)
        then ''
          session=$(loginctl list-sessions | grep $USER | awk '{print $1}')
          until [[ "$(loginctl show-session -p State $session)" = "State=active" ]]; do sleep 60; done
          until [[ "$(loginctl show-session -p LockedHint $session)" = "LockedHint=no" ]]; do sleep 60; done
        ''
        else "";
      script = ''
        for d in /home/$USER/*; do
          if [ -d "$d" ]; then
            rsync -Pav --delete "$d" /nfs/abyss/$USER/.
          fi
        done
      '';
      wantedBy = [];
    };
  };
}
