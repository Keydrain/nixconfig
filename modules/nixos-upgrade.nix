{
  config,
  lib,
  pkgs,
  variables,
  ...
}:
with lib; let
  cfg = config.keydrain.nixos-upgrade;
  username = variables.primaryUser;
in {
  options = {
    keydrain.nixos-upgrade.enable =
      mkEnableOption "Enable keydrain's nixos-upgrade configuration";
    keydrain.nixos-upgrade.operation = mkOption {
      type = types.str;
      default = "boot";
      description = "Configure the operation for keydrain's nixos-upgrade configuration";
    };
    keydrain.nixos-upgrade.reboot = mkOption {
      type = types.bool;
      default = false;
      description = "Configure auto reboot for keydrain's nixos-upgrade configuration";
    };
    keydrain.nixos-upgrade.notifications = mkOption {
      type = types.bool;
      default = false;
      description = "Enable notifications for keydrain's nixos-upgrade configuration";
    };
    keydrain.nixos-upgrade.timers.enable = mkOption {
      type = types.bool;
      default = true;
      description = "Enable timers for keydrain's nixos-upgrade configuration";
    };
    keydrain.nixos-upgrade.paths = mkOption {
      type = types.bool;
      default = true;
      description = "Enable paths for keydrain's nixos-upgrade configuration";
    };
    keydrain.nixos-upgrade.timers.calendar = mkOption {
      type = types.str;
      default = "weekly";
      description = "Configure the calendar frequency for timers for keydrain's nixos-upgrade configuration";
    };
    keydrain.nixos-upgrade.timers.delaySec = mkOption {
      type = types.str;
      default = "604800";
      description = "Configure the calendar delay for timers for keydrain's nixos-upgrade configuration";
    };
  };

  config = mkIf cfg.enable {
    systemd.timers.nixos-upgrade = mkIf cfg.timers.enable {
      timerConfig = {
        OnCalendar = cfg.timers.calendar;
        Persistent = true;
        RandomizedDelaySec = cfg.timers.delaySec;
      };
      wantedBy = ["multi-user.target"];
    };

    systemd.paths.nixos-upgrade = mkIf cfg.paths {
      pathConfig.PathChanged = ["/etc/nixos/flake.nix" "/etc/nixos/flake.lock"];
      wantedBy = ["multi-user.target"];
    };

    systemd.services.nixos-upgrade = {
      unitConfig = {
        After = "network-online.target";
        Requisite = "network-online.target";
        X-StopOnRemoval = false;
        OnFailure =
          if (cfg.notifications)
          then "notify@failure-nixos-upgrade-NixOS\\x20Upgrade.service"
          else "";
        OnSuccess =
          if (cfg.notifications)
          then "notify@success-nixos-upgrade-NixOS\\x20Upgrade.service"
          else "";
        Wants =
          if (cfg.notifications)
          then "notify@start-nixos-upgrade-NixOS\\x20Upgrade.service"
          else "";
      };
      restartIfChanged = false;
      serviceConfig = {Type = "oneshot";};
      environment =
        config.nix.envVars
        // {
          inherit (config.environment.sessionVariables) NIX_PATH;
          HOME = "/root";
          SHELL = "/run/current-system/sw/bin/bash";
        }
        // config.networking.proxy.envVars;
      path = with pkgs; [
        config.nix.package.out
        coreutils
        findutils
        gawk
        git
        git-crypt
        gnugrep
        gnutar
        gzip
        xz.bin
      ];
      preStart =
        if (cfg.notifications)
        then ''
          session=$(loginctl list-sessions | grep ${username} | awk '{print $1}')
          until [[ "$(loginctl show-session -p State $session)" = "State=active" ]]; do sleep 60; done
          until [[ "$(loginctl show-session -p LockedHint $session)" = "LockedHint=no" ]]; do sleep 60; done
        ''
        else "";
      script = let
        nixos-rebuild = "${config.system.build.nixos-rebuild}/bin/nixos-rebuild";
        nixos-flags = "--show-trace --verbose";
        git-check = "git --git-dir=/home/${username}/git/nixconfig/.git --work-tree=. -C /home/${username}/git/nixconfig diff-index --quiet HEAD --";
      in
        if (cfg.reboot)
        then ''
          git config --global --add safe.directory /home/${username}/git/nixconfig
          if ${git-check}; then
            ${nixos-rebuild} boot ${nixos-flags}
            booted="$(readlink /run/booted-system/{initrd,kernel,kernel-modules})"
            built="$(readlink /nix/var/nix/profiles/system/{initrd,kernel,kernel-modules})"
            if [ "$booted" = "$built" ]; then
              ${nixos-rebuild} switch ${nixos-flags}
            else
              /run/current-system/sw/bin/shutdown -r +1
            fi
          fi
        ''
        else ''
          git config --global --add safe.directory /home/${username}/git/nixconfig
          if ${git-check}; then
            ${nixos-rebuild} ${cfg.operation} ${nixos-flags}
          fi
        '';
      wantedBy = [];
    };
  };
}
