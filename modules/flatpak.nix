{
  config,
  lib,
  pkgs,
  variables,
  ...
}:
with lib; let
  cfg = config.keydrain.flatpak;
in {
  options = {
    keydrain.flatpak.enable =
      mkEnableOption "Enable keydrain's flatpak configuration";
  };

  config = mkIf cfg.enable {
    services.flatpak.enable = true;

    system.activationScripts.flatpak-setup = {
      text = ''
        ${pkgs.flatpak}/bin/flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
        ${pkgs.flatpak}/bin/flatpak remote-add --user --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
      '';
      deps = ["user-setup"];
    };

    users.users.${variables.primaryUser} = {extraGroups = ["flatpak"];};
  };
}
