{
  config,
  lib,
  ...
}:
with lib; let
  cfg = config.keydrain.locale;
in {
  options = {
    keydrain.locale.enable =
      mkEnableOption "Enable keydrain's locale configuration";
  };

  config = mkIf cfg.enable {i18n.defaultLocale = "en_US.UTF-8";};
}
