{
  config,
  lib,
  pkgs,
  variables,
  ...
}:
with lib; let
  cfg = config.keydrain.nix-gc;
in {
  options = {
    keydrain.nix-gc.enable =
      mkEnableOption "Enable keydrain's nix-gc configuration";
    keydrain.nix-gc.notifications = mkOption {
      type = types.bool;
      default = false;
      description = "Enable notifications for keydrain's nix-gc configuration";
    };
    keydrain.nix-gc.retention = mkOption {
      type = types.str;
      default = "90";
      description = "Enable retention days for keydrain's nix-gc configuration";
    };
    keydrain.nix-gc.timers.enable = mkOption {
      type = types.bool;
      default = true;
      description = "Enable timers for keydrain's nix-gc configuration";
    };
    keydrain.nix-gc.timers.calendar = mkOption {
      type = types.str;
      default = "daily";
      description = "Configure the calendar frequency for timers for keydrain's nix-gc configuration";
    };
    keydrain.nix-gc.timers.delaySec = mkOption {
      type = types.str;
      default = "86400";
      description = "Configure the calendar delay for timers for keydrain's nix-gc configuration";
    };
  };

  config = mkIf cfg.enable {
    nix = {
      settings.auto-optimise-store = true;
      extraOptions = ''
        min-free = ${toString (10 * 1024 * 1024 * 1024)}
        max-free = ${toString (50 * 1024 * 1024 * 1024)}
      '';
    };

    systemd.timers.nix-gc = mkIf cfg.timers.enable {
      timerConfig = {
        OnCalendar = cfg.timers.calendar;
        Persistent = true;
        RandomizedDelaySec = cfg.timers.delaySec;
      };
      wantedBy = ["multi-user.target"];
    };

    systemd.services.nix-gc = {
      unitConfig = {
        OnFailure =
          if (cfg.notifications)
          then "notify@failure-nix-gc-Nix\\x20Garbage\\x20Collection.service"
          else "";
        OnSuccess =
          if (cfg.notifications)
          then "notify@success-nix-gc-Nix\\x20Garbage\\x20Collection.service"
          else "";
        Wants =
          if (cfg.notifications)
          then "notify@start-nix-gc-Nix\\x20Garbage\\x20Collection.service"
          else "";
      };
      restartIfChanged = false;
      path = with pkgs; [
        coreutils
        findutils
        gawk
        gnugrep
        gnused
        gnutar
        nix
        util-linux
      ];
      environment.SHELL = "/run/current-system/sw/bin/bash";
      preStart =
        if (cfg.notifications)
        then ''
          session=$(loginctl list-sessions | grep ${variables.primaryUser} | awk '{print $1}')
          until [[ "$(loginctl show-session -p State $session)" = "State=active" ]]; do sleep 60; done
          until [[ "$(loginctl show-session -p LockedHint $session)" = "LockedHint=no" ]]; do sleep 60; done
        ''
        else "";
      script = let
        numdays = cfg.retention;
      in
        lib.mkForce ''
          runuser -l ${variables.primaryUser} -c 'nix-env --delete-generations ${numdays}d'
          runuser -l ${variables.primaryUser} -c 'nix-collect-garbage --delete-older-than ${numdays}d'
          nix-env --delete-generations ${numdays}d
          nix-collect-garbage --delete-older-than ${numdays}d
        '';
      wantedBy = [];
    };
  };
}
