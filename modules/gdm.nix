{
  config,
  lib,
  variables,
  ...
}:
with lib; let
  cfg = config.keydrain.gdm;
in {
  options = {
    keydrain.gdm.enable = mkEnableOption "Enable keydrain's gdm configuration";
    keydrain.gdm.autoLogin = mkOption {
      type = types.bool;
      default = false;
      description = "Enable autoLogin on keydrain's gdm configuration";
    };
  };

  config = mkIf cfg.enable {
    services.xserver.displayManager.gdm.enable = true;
    services.xserver.displayManager.gdm.wayland = true;
    services.displayManager.autoLogin =
      if (cfg.autoLogin)
      then {
        enable = true;
        user = variables.primaryUser;
      }
      else {
        enable = false;
      };
  };
}
