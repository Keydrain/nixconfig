{
  config,
  lib,
  pkgs,
  variables,
  ...
}:
with lib; let
  cfg = config.keydrain.libvirtd;
in {
  options = {
    keydrain.libvirtd.enable =
      mkEnableOption "Enable keydrain's libvirtd configuration";
    # keydrain.libvirtd.useDHCP = mkOption {
    #   type = types.nullOr types.bool;
    #   default = null;
    #   description = "Enable dhcp on keydrain's libvirtd configuration";
    # };
  };

  config = mkIf cfg.enable {
    environment.systemPackages = with pkgs; [
      looking-glass-client
      OVMF
      qemu
      # qemu.overrideAttrs
      # (oldAttrs: {
      #   configureFlags =
      #     [ "--target-list=aarch64-linux,x86_64-linux" ];
      # })
      qemu_kvm
      spice
      spice-gtk
      spice-vdagent
      virt-manager
      virt-viewer
      libguestfs
    ];

    # Enable DHCP on virbr0
    # networking.interfaces.virbr0.useDHCP = cfg.useDHCP;

    # Libvirt
    virtualisation.libvirtd.enable = true;
    programs.dconf.enable = true;

    # Ensure localized scripts have a destination
    system.activationScripts = {
      libvirt-setup = {
        text = ''
          mkdir -p /home/${variables.primaryUser}/.local/lib/libvirt
          mkdir -p /home/${variables.primaryUser}/.local/share/libvirt
          mkdir -p /home/${variables.primaryUser}/.local/etc/libvirt
        '';
        deps = ["user-setup"];
      };
    };

    users.users.${variables.primaryUser} = {
      extraGroups = [
        "libvirtd" # LibVirt
        "qemu-libvirtd" # Qemu
        "kvm" # KVM
      ];
    };
  };
}
