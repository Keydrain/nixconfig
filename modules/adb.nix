{
  config,
  lib,
  pkgs,
  ...
}:
with lib; let
  cfg = config.keydrain.adb;
  username = variables.primaryUser;
  userid = config.users.users.${username}.gid;
  phone-path = "/run/user/${userid}/gvfs/mtp:host=${variables.userAndroidPhoneID}";
in {
  options = {
    keydrain.adb.enable = mkEnableOption "Enable keydrain's adb configuration";
  };

  config = mkIf cfg.enable {
    environment.systemPackages = with pkgs; [
      # adb-sync
    ];

    # Enable Android debugging
    programs.adb.enable = true;

    users.users.${variables.primaryUser} = {extraGroups = ["adbusers"];};

    systemd = {
      services = {
        android-backup = let
          hostname = config.networking.hostName;
        in {
          enable = false;
          unitConfig = {
            OnFailure = "notify@failure-android-backup-Android\\x20Backup.service";
            Wants = "notify@start-android-backup-Android\\x20Backup.service";
          };
          postStart = ''
            systemctl start notify@success-android-backup-Android\\x20Backup.service
          '';
          serviceConfig = {
            Type = "oneshot";
            RuntimeDirectory = "/home/${username}/android-backup";
            User = "${username}";
            Group = "users";
            CPUSchedulingPolicy = "idle";
            IOSchedulingClass = "idle";
            PrivateTmp = true;
          };
          environment = {SHELL = "/run/current-system/sw/bin/bash";};
          path = with pkgs; [coreutils adb gnugrep gawk];
          preStart = ''
            session=$(loginctl list-sessions | grep ${variables.primaryUser} | awk '{print $1}')
            until [[ "$(loginctl show-session -p State $session)" = "State=active" ]]; do sleep 60; done
            until [[ "$(loginctl show-session -p LockedHint $session)" = "LockedHint=no" ]]; do sleep 60; done
          '';
          script = ''
            adb backup
          '';
          wantedBy = [];
        };
      };
      paths.android-backup = {
        enable = false;
        pathConfig = {DirectoryNotEmpty = ["${phone-path}"];};
        wantedBy = ["multi-user.target"];
      };
    };
  };
}
