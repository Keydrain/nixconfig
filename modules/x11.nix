{
  config,
  lib,
  ...
}:
with lib; let
  cfg = config.keydrain.x11;
in {
  options = {
    keydrain.x11.enable = mkEnableOption "Enable keydrain's x11 configuration";
  };

  config = mkIf cfg.enable {
    services.xserver.enable = true;
    services.xserver.xkb.layout = "us";
  };
}
