{
  config,
  lib,
  variables,
  ...
}:
with lib; let
  cfg = config.keydrain.lightdm;
in {
  options = {
    keydrain.lightdm.enable =
      mkEnableOption "Enable keydrain's lightdm configuration";
    keydrain.lightdm.autoLogin = mkOption {
      type = types.bool;
      default = false;
      description = "Enable autoLogin on keydrain's lightdm configuration";
    };
  };

  config = mkIf cfg.enable {
    services.xserver.displayManager.lightdm.enable = true;
    services.displayManager.autoLogin =
      if (cfg.autoLogin)
      then {
        enable = true;
        user = variables.primaryUser;
      }
      else {
        enable = false;
      };
  };
}
