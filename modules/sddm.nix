{
  config,
  lib,
  variables,
  ...
}:
with lib; let
  cfg = config.keydrain.sddm;
in {
  options = {
    keydrain.sddm.enable =
      mkEnableOption "Enable keydrain's sddm configuration";
    keydrain.sddm.autoLogin = mkOption {
      type = types.bool;
      default = false;
      description = "Enable autoLogin on keydrain's sddm configuration";
    };
  };

  config = mkIf cfg.enable {
    services.xserver.displayManager.sddm.enable = true;
    services.displayManager.autoLogin =
      if (cfg.autoLogin)
      then {
        enable = true;
        user = variables.primaryUser;
      }
      else {
        enable = false;
      };
  };
}
