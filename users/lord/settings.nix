{
  pkgs,
  variables,
  ...
}: {
  keydrain = {
    k3s = {
      enable = true;
      labels = "infra=nuc";
    };
    nix-gc.retention = "7";
    nixconfig = {
      enable = true;
      timers = {
        calendar = "hourly";
        delaySec = "3600";
      };
    };
    nixos-upgrade = {
      enable = true;
      operation = "switch";
      reboot = true;
      timers = {
        calendar = "daily";
        delaySec = "86400";
      };
    };
    openssh.enable = true;
    zerotier.enable = false;
  };

  # Add home-manager shell to accepted shells
  environment.shells = ["/etc/profiles/per-user/lord/bin/zsh"];

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users = {
    mutableUsers = true; # Ensure users can change password
    users.lord = {
      uid = 1000;
      isNormalUser = true;
      shell = "/etc/profiles/per-user/lord/bin/zsh";
      extraGroups = [
        "wheel" # Sudo
        "networkManager" # Add networks
      ];
      initialPassword = "nixos";
    };
  };

  # Set primary user variable
  variables.primaryUser = "lord";

  # Setup .local dirs
  system.activationScripts = {
    user-setup.text = ''
      mkdir -p /home/lord/.local/bin
      mkdir -p /home/lord/.local/lib
      mkdir -p /home/lord/.local/share
      mkdir -p /home/lord/.local/etc
      chgrp -R users /home/lord/.local
      chown -R lord /home/lord/.local
    '';
  };
}
