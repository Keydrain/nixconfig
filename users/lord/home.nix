{pkgs, ...}: {
  keydrain = {
    agenix.enable = true;
    usbutils.enable = true;
  };

  home = {
    file = {};
    homeDirectory = "/home/lord";
    keyboard = {layout = "us";};
    packages = with pkgs; [nfs-utils];
    sessionVariables = {};
    username = "lord";
  };

  programs = {home-manager.enable = true;};
}
