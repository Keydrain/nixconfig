{variables, ...}: {
  keydrain = {
    borgbackup.enable = true;
    fprintd.enable = true;
    nixconfig = {
      enable = true;
      notifications = false;
    };
    nixos-upgrade = {
      enable = true;
      notifications = false;
    };
    rsync-backup.enable = true;
    zerotier.enable = false;
  };

  # Add home-manager shell to accepted shells
  environment.shells = ["/etc/profiles/per-user/darlene/bin/zsh"];

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users = {
    mutableUsers = true; # Ensure users can change password
    users.darlene = {
      uid = 1000;
      isNormalUser = true;
      shell = "/etc/profiles/per-user/darlene/bin/zsh";
      extraGroups = [
        "wheel" # Sudo
        "networkManager" # Add networks
      ];
      initialPassword = "nixos";
    };
  };

  # Set primary user variable
  variables.primaryUser = "darlene";

  # Setup .local dirs
  system.activationScripts = {
    user-setup.text = ''
      mkdir -p /home/darlene/.local/bin
      mkdir -p /home/darlene/.local/lib
      mkdir -p /home/darlene/.local/share
      mkdir -p /home/darlene/.local/etc
      chgrp -R users /home/darlene/.local
      chown -R darlene /home/darlene/.local
    '';
  };
}
