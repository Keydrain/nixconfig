{pkgs, ...}: {
  keydrain = {
    chromium.enable = true;
    dislocker.enable = true;
    firefox.enable = true;
    gimp.enable = true;
    gthumb.enable = true;
    libreoffice.enable = true;
    vlc.enable = true;
  };

  home = {
    file = {};
    homeDirectory = "/home/darlene";
    keyboard = {layout = "us";};
    packages = with pkgs; [
      unstable.brave
      shotwell
      libheif
    ];
    sessionVariables = {};
    username = "darlene";
  };

  programs = {home-manager.enable = true;};
}
