{
  pkgs,
  variables,
  ...
}: {
  keydrain = {
    adb.enable = false; # TODO: Still testing
    borgbackup.enable = true;
    docker.enable = true;
    # gnupg.enable = true;
    flatpak.enable =
      false; # TODO: Seems to make a call at startup to flathub that fails sometimes
    fprintd.enable = true;
    kubectl.enable = true;
    libvirtd.enable = true;
    nix-gc = {
      notifications = true;
      retention = "21";
    };
    nixconfig.enable = true;
    nixos-upgrade = {
      enable = true;
      paths = false;
    };
    openssh.enable = true;
    qFlipper.enable = true;
    steam.enable = true;
    sysstat.enable = true;
    rsync-backup.enable = true;
    zerotier.enable = false;
  };

  # TODO: https://github.com/NixOS/nixpkgs/issues/118612
  # environment.systemPackages = with pkgs; [ gnomeExtensions.fly-pie ];

  # Add home-manager shell to accepted shells
  environment.shells = ["/etc/profiles/per-user/clint/bin/zsh"];

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users = {
    mutableUsers = true; # Ensure users can change password
    users.clint = {
      uid = 1000;
      isNormalUser = true;
      shell = "/etc/profiles/per-user/clint/bin/zsh";
      extraGroups = [
        "wheel" # Sudo
        "networkManager" # Add networks
        "lp" # Printers
      ];
      initialPassword = "nixos";
    };
  };

  # Set primary user variable
  variables.primaryUser = "clint";

  # Setup .local dirs
  system.activationScripts = {
    user-setup.text = ''
      mkdir -p /home/clint/.local/bin
      mkdir -p /home/clint/.local/lib
      mkdir -p /home/clint/.local/share
      mkdir -p /home/clint/.local/etc
      chgrp -R users /home/clint/.local
      chown -R clint /home/clint/.local
    '';
  };
}
