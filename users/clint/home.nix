{
  config,
  pkgs,
  lib,
  ...
}: {
  keydrain = {
    agenix.enable = true;
    audacity.enable = true;
    chromium.enable = true;
    discord.enable = true;
    dislocker.enable = true;
    easyeffects.enable = true;
    ffmpeg.enable = true;
    firefox.enable = true;
    gimp.enable = true;
    gnupg.enable = true;
    go.enable = true;
    godot.enable = true;
    gource.enable = true;
    gthumb.enable = true;
    handbrake.enable = true;
    inkscape.enable = true;
    jq.enable = true;
    libreoffice.enable = true;
    lutris.enable = true;
    minecraft.enable = true;
    obsidian.enable = true;
    openmw.enable = true;
    pciutils.enable = true;
    rpi-imager.enable = true;
    rust.enable = true;
    shotcut.enable = true;
    skopeo.enable = true;
    todoist.enable = true;
    usbutils.enable = true;
    vlc.enable = true;
    vscodium.enable = true;
    wesnoth.enable = true;
    win10-init.enable = true;
    yamllint.enable = true;
    zip.enable = true;
    zoom.enable = true;
  };

  home = {
    file = {};
    homeDirectory = "/home/clint";
    keyboard = {layout = "us";};
    packages = with pkgs; [
      master.prusa-slicer
      openshot-qt
      hakuneko
      gotop
      zathura
      # unstable.hyperspace-cli
      unstable.brave
      freecad
      openscad
      leocad
      blender
      cmatrix
      dconf2nix
      signal-desktop
      ycmd # Go related?
      plex-media-player
      ddrescue
      testdisk
      wally-cli # Keybaord programming
      calibre
      deluge
      qbittorrent
      rtorrent
      google-chrome
      nfs-utils
      apacheHttpd
      tidal-hifi
      docker-compose
      opendrop
      yq
      shotwell
      libheif
    ];
    sessionVariables = {};
    username = "clint";
  };

  programs = {
    home-manager.enable = true;
    chromium = {
      extensions = [
        {id = "homgcnaoacgigpkkljjjekpignblkeae";} # appspector
        {id = "eimadpbcbfnmbkopoojfekhnkhdbieeh";} # dark reader
        {id = "iblijlcdoidgdpfknkckljiocdbnlagk";} # goo.gl url shortener
        {id = "laookkfknpbbblfpciffpaejjkokdgca";} # momentum
        {id = "kbmfpngjjgdllneeigpgjifpgocmfgmb";} # reddit enhancement suite
        {id = "bjmimocndihpmdoelbiilpkkfkppikap";} # jointabs
        {id = "cdochbecpfdpjobpgnacnbepkgcfhoek";} # tab scissors
        {id = "ghnomdcacenbmilgjigehppbamfndblo";} # the camelizer
        {id = "okadibdjfemgnhjiembecghcbfknbfhg";} # enhanced steam
        {id = "kdbmhfkmnlmbkgbabkdealhhbfhlmmon";} # steam database
      ];
    };
    firefox.profiles.default = {
      extensions = with config.nur.repos.rycee.firefox-addons; [
        darkreader
        reddit-enhancement-suite
      ];
    };
  };
}
