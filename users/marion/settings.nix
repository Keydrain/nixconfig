{variables, ...}: {
  keydrain = {
    borgbackup.enable = true;
    fprintd.enable = true;
    nixconfig = {
      enable = true;
      notifications = false;
    };
    nixos-upgrade = {
      enable = true;
      notifications = false;
    };
    rsync-backup.enable = true;
    zerotier.enable = false;
  };

  # Add home-manager shell to accepted shells
  environment.shells = ["/etc/profiles/per-user/marion/bin/zsh"];

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users = {
    mutableUsers = true; # Ensure users can change password
    users.marion = {
      uid = 1000;
      isNormalUser = true;
      shell = "/etc/profiles/per-user/marion/bin/zsh";
      extraGroups = [
        "wheel" # Sudo
        "networkManager" # Add networks
      ];
      initialPassword = "nixos";
    };
  };

  # Set primary user variable
  variables.primaryUser = "marion";

  # Setup .local dirs
  system.activationScripts = {
    user-setup.text = ''
      mkdir -p /home/marion/.local/bin
      mkdir -p /home/marion/.local/lib
      mkdir -p /home/marion/.local/share
      mkdir -p /home/marion/.local/etc
      chgrp -R users /home/marion/.local
      chown -R marion /home/marion/.local
    '';
  };
}
