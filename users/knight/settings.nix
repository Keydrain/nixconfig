{
  pkgs,
  variables,
  ...
}: {
  keydrain = {
    k3s = {
      enable = true;
      labels = "infra=rpi";
    };
    nix-gc.retention = "7";
    nixconfig = {
      enable = true;
      timers = {
        calendar = "hourly";
        delaySec = "3600";
      };
    };
    nixos-upgrade = {
      enable = true;
      operation = "switch";
      reboot = true;
      timers = {
        calendar = "daily";
        delaySec = "86400";
      };
    };
    openssh.enable = true;
    U6143_ssd1306.enable = true;
    zerotier.enable = false;
  };

  # Add home-manager shell to accepted shells
  environment.shells = ["/etc/profiles/per-user/knight/bin/zsh"];

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users = {
    mutableUsers = true; # Ensure users can change password
    users.knight = {
      uid = 1000;
      isNormalUser = true;
      shell = "/etc/profiles/per-user/knight/bin/zsh";
      extraGroups = [
        "wheel" # Sudo
        "networkManager" # Add networks
      ];
      initialPassword = "nixos";
    };
  };

  # Set primary user variable
  variables.primaryUser = "knight";

  # Setup .local dirs
  system.activationScripts = {
    user-setup.text = ''
      mkdir -p /home/knight/.local/bin
      mkdir -p /home/knight/.local/lib
      mkdir -p /home/knight/.local/share
      mkdir -p /home/knight/.local/etc
      chgrp -R users /home/knight/.local
      chown -R knight /home/knight/.local
    '';
  };
}
