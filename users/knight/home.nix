{pkgs, ...}: {
  keydrain = {
    agenix.enable = true;
    usbutils.enable = true;
  };

  home = {
    file = {};
    homeDirectory = "/home/knight";
    keyboard = {layout = "us";};
    packages = with pkgs; [nfs-utils];
    sessionVariables = {};
    username = "knight";
  };

  programs = {home-manager.enable = true;};
}
