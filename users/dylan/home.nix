{pkgs, ...}: {
  keydrain = {
    discord.enable = true;
    firefox.enable = true;
    gthumb.enable = true;
    jq.enable = true;
    libreoffice.enable = true;
    lutris.enable = true;
    minecraft.enable = true;
    openmw.enable = true;
    pciutils.enable = true;
    shotcut.enable = true;
    vlc.enable = true;
    win10-init.enable = true;
    zip.enable = true;
    zoom.enable = false;
  };

  home = {
    file = {};
    homeDirectory = "/home/dylan";
    keyboard = {layout = "us";};
    packages = with pkgs; [
      autorandr
      openshot-qt
      master.prusa-slicer
      unstable.brave
    ];

    sessionVariables = {};
    username = "dylan";
  };

  programs = {home-manager.enable = true;};
}
