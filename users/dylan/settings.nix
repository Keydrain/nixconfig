{variables, ...}: {
  keydrain = {
    borgbackup.enable = true;
    libvirtd.enable = true;
    nixconfig.enable = true;
    nixos-upgrade.enable = true;
    nvidia.enable = true;
    openssh.enable = true;
    steam.enable = true;
    zerotier.enable = false;
  };

  # Add home-manager shell to accepted shells
  environment.shells = ["/etc/profiles/per-user/dylan/bin/zsh"];

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users = {
    mutableUsers = true; # Ensure users can change password
    users.dylan = {
      uid = 1000;
      isNormalUser = true;
      shell = "/etc/profiles/per-user/dylan/bin/zsh";
      extraGroups = [
        "wheel" # Sudo
        "networkManager" # Add networks
        "scanner"
        "lp"
      ];
      initialPassword = "nixos";
    };
  };

  # Set primary user variable
  variables.primaryUser = "dylan";

  # Setup .local dirs
  system.activationScripts = {
    user-setup.text = ''
      mkdir -p /home/dylan/.local/bin
      mkdir -p /home/dylan/.local/lib
      mkdir -p /home/dylan/.local/share
      mkdir -p /home/dylan/.local/etc
      chgrp -R users /home/dylan/.local
      chown -R dylan /home/dylan/.local
    '';
  };
}
