{
  pkgs,
  variables,
  ...
}: {
  keydrain = {
    nixconfig = {
      enable = true;
      timers = {
        calendar = "hourly";
        delaySec = "3600";
      };
    };
  };

  # Add home-manager shell to accepted shells
  environment.shells = ["/etc/profiles/per-user/nixos/bin/zsh"];
  users = {
    mutableUsers = false;
    users.nixos = {
      uid = 1000;
      isNormalUser = true;
      shell = "/etc/profiles/per-user/nixos/bin/zsh";
      extraGroups = [
        "wheel" # Sudo
        "networkManager" # Add networks
      ];
      hashedPassword = "$6$RW26SBrh/6WbIYvX$A8SAZdQaZEwlScLex8P6n.PDonY2ILqRJF1JkHpX3tg4dIYpSt.Q1pC/RP9XSemfyAQuzRX27t4KQEgSvvWGh.";
    };
  };

  # Set primary user variable
  variables.primaryUser = "nixos";

  # Setup .local dirs
  system.activationScripts = {
    user-setup.text = ''
      mkdir -p /home/nixos/.local/bin
      mkdir -p /home/nixos/.local/lib
      mkdir -p /home/nixos/.local/share
      mkdir -p /home/nixos/.local/etc
      chgrp -R users /home/nixos/.local
      chown -R nixos /home/nixos/.local
    '';
  };
}
