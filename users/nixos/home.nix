{pkgs, ...}: {
  keydrain = {
    pciutils.enable = true;
    usbutils.enable = true;
  };

  home = {
    file = {};
    homeDirectory = "/home/nixos";
    keyboard = {layout = "us";};
    packages = with pkgs; [];
    sessionVariables = {};
    username = "nixos";
  };

  programs = {home-manager.enable = true;};
}
