{pkgs, ...}:
with pkgs; let
  haproxy = dockerTools.buildImage {
    name = "haproxy";
    tag = "latest";
    runAsRoot = ''
      #!${pkgs.runtimeShell}
      ${pkgs.dockerTools.shadowSetup}
      groupadd -r haproxy
      useradd -r -g haproxy haproxy
    '';
    copyToRoot = pkgs.buildEnv {
      name = "image-root";
      paths = [pkgs.haproxy];
      pathsToLink = ["/bin" "/etc" "/var"];
    };
    created = "now";
    config = {
      User = "haproxy:haproxy";
      Cmd = ["haproxy"];
    };
  };

  nginx = dockerTools.buildImage {
    name = "nginx";
    tag = "latest";
    runAsRoot = ''
      #!${pkgs.runtimeShell}
      ${pkgs.dockerTools.shadowSetup}
      groupadd -r nginx
      useradd -r -g nginx nginx
      mkdir -p /var/log/nginx
      touch /var/log/nginx/error.log
      chmod 766 /var/log/nginx/error.log
    '';
    copyToRoot = pkgs.buildEnv {
      name = "image-root";
      paths = [pkgs.nginx];
      pathsToLink = ["/bin" "/etc" "/var"];
    };
    created = "now";
    config = {
      User = "nginx:nginx";
      Cmd = ["nginx"];
    };
  };

  haproxy-k3s = dockerTools.buildImage {
    name = "haproxy-k3s";
    tag = "latest";
    fromImage = haproxy;
    created = "now";
    config = let
      haproxyCfg =
        pkgs.writeText "haproxy.cfg" (builtins.readFile ./haproxy-k3s.cfg);
    in {
      Cmd = ["haproxy" "-f" haproxyCfg "-W" "-V" "-db"];
      ExposedPorts = {"6443/tcp" = {};};
    };
  };
in rec {
  inherit
    haproxy
    nginx
    haproxy-k3s
    ;
}
