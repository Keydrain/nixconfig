{
  flake-utils-plus,
  home-manager,
  dotfiles,
  lib,
  ...
}: let
  nixosModules =
    flake-utils-plus.lib.exportModules
    (lib.mapAttrsToList (name: value: ./modules + ("/" + name)) (lib.filterAttrs
      (key: value: value == "regular" && lib.hasSuffix ".nix" key)
      (builtins.readDir ./modules)));

  homeModules =
    flake-utils-plus.lib.exportModules
    (lib.mapAttrsToList (name: value: ./programs + ("/" + name))
      (lib.filterAttrs
        (key: value: value == "regular" && lib.hasSuffix ".nix" key)
        (builtins.readDir ./programs)));

  sharedNixosModules = with nixosModules; [
    {system.stateVersion = "23.05";}
    adb
    anbox
    apparmor
    borgbackup
    cachix
    dnsutils
    docker
    dosfstools
    firewall
    flake
    flatpak
    fonts
    fprintd
    fwupd
    gdm
    gnome
    gnupg
    k3s
    kubectl
    libvirtd
    lightdm
    locale
    lxd
    nixconfig
    nix-gc
    nixos-upgrade
    nvidia
    openssh
    pipewire
    rsync-backup
    U6143_ssd1306
    qFlipper
    sddm
    steam
    systemd-notifications
    sysstat
    variables
    x11
    xdg-utils
    zerotier
  ];

  sharedHomeModules = with homeModules; [
    {home.stateVersion = "23.05";}
    agenix
    alejandra
    audacity
    chromium
    chuck
    curl
    discord
    dislocker
    easyeffects
    efibootmgr
    ffmpeg
    file
    firefox
    gimp
    gnupg
    git
    dmidecode
    dotfiles.nixosModules.git
    go
    godot
    gource
    gthumb
    handbrake
    htop
    dotfiles.nixosModules.htop
    inkscape
    jq
    julia
    kitty
    dotfiles.nixosModules.kitty
    libreoffice
    lutris
    minecraft
    neofetch
    nixfmt
    nix-index
    notable
    nur
    obsidian
    obs-studio
    openmw
    parted
    pciutils
    perl
    python
    rpi-imager
    rust
    shotcut
    skopeo
    slack
    starship
    dotfiles.nixosModules.starship
    supertuxkart
    supertux
    todoist
    tree
    usbutils
    vim
    dotfiles.nixosModules.vim
    vlc
    vscodium
    wesnoth
    wget
    win10-init
    yamllint
    dotfiles.nixosModules.yamllint
    zeroad
    zip
    zoom
    zsh
    dotfiles.nixosModules.zsh
  ];

  homeDefaults =
    sharedNixosModules
    ++ [
      {
        home-manager = {
          useGlobalPkgs = lib.mkDefault true;
          useUserPackages = lib.mkDefault true;
          sharedModules =
            sharedHomeModules
            ++ [
              ({...}: {
                keydrain = {
                  alejandra.enable = lib.mkDefault true;
                  curl.enable = lib.mkDefault true;
                  dmidecode.enable = lib.mkDefault true;
                  file.enable = lib.mkDefault true;
                  git.enable = lib.mkDefault true;
                  htop.enable = lib.mkDefault true;
                  neofetch.enable = lib.mkDefault true;
                  nix-index.enable = lib.mkDefault true;
                  nixfmt.enable = lib.mkDefault false;
                  starship.enable = lib.mkDefault true;
                  tree.enable = lib.mkDefault true;
                  vim.enable = lib.mkDefault true;
                  wget.enable = lib.mkDefault true;
                  zsh.enable = lib.mkDefault true;
                };
              })
            ];
        };
      }
    ];

  nixosHeadlessDefaults =
    homeDefaults
    ++ [
      home-manager.nixosModules.home-manager
      ({...}: {
        keydrain = {
          apparmor.enable = lib.mkDefault true;
          cachix.enable = lib.mkDefault true;
          dnsutils.enable = lib.mkDefault true;
          dosfstools.enable = lib.mkDefault true;
          firewall.enable = lib.mkDefault true;
          flake.enable = lib.mkDefault true;
          fonts.enable = lib.mkDefault true;
          fwupd.enable = lib.mkDefault true;
          locale.enable = lib.mkDefault true;
          nix-gc.enable = lib.mkDefault true;
        };
      })
    ]
    ++ [
      {
        home-manager = {
          sharedModules = [
            ({...}: {
              keydrain = {
                file.enable = lib.mkDefault true;
                nur.enable = lib.mkDefault true;
                parted.enable = lib.mkDefault true;
              };
            })
          ];
        };
      }
    ];

  nixosGuiDefaults =
    nixosHeadlessDefaults
    ++ [
      ({...}: {
        keydrain = {
          borgbackup.notifications = lib.mkDefault true;
          gdm.enable = lib.mkDefault true;
          gnome.enable = lib.mkDefault true;
          nixconfig.notifications = lib.mkDefault true;
          nixos-upgrade.notifications = lib.mkDefault true;
          pipewire.enable = lib.mkDefault true;
          rsync-backup.notifications = lib.mkDefault true;
          x11.enable = lib.mkDefault true;
          xdg-utils.enable = lib.mkDefault true;
        };
        # nixpkgs.overlays = [
        #   (self: super: {
        #     discord = super.discord.overrideAttrs (_: {
        #       src = builtins.fetchTarball
        #         "https://dl.discordapp.net/apps/linux/0.0.17/discord-0.0.17.tar.gz";
        #     });
        #   })
        # ];
      })
    ]
    ++ [
      {
        home-manager = {
          sharedModules = [
            ({...}: {
              keydrain = {
                efibootmgr.enable = lib.mkDefault true;
                firefox.enable = lib.mkDefault true;
                libreoffice.enable = lib.mkDefault true;
                kitty.enable = lib.mkDefault true;
                parted.gui = lib.mkDefault true;
                vlc.enable = lib.mkDefault true;
              };
            })
          ];
        };
      }
    ];
in {
  inherit
    nixosModules
    homeModules
    sharedNixosModules
    sharedHomeModules
    homeDefaults
    nixosHeadlessDefaults
    nixosGuiDefaults
    ;
}
