# Keydrain's nixconfig

## Disclaimer

~~I don't know what I'm doing.~~
I _might_ have an idea what I'm doing.

This is still a work in progress and any suggestions or improvements are welcome.

Checkout the list of [resources](https://gitlab.com/Keydrain/nixconfig#resources) that I've leaned on while working on this repo.

## Description

Nix related configurations for NixOS and Nix

This project is organized into categories for easy pick and play.

### Machines

Each machine (the physical hardware) needs a definition that governs things related to said hardware.
What drives to mount and where, what kernel mods, etc.
Only one machine definition can exist per physical machine in the flake.nix file.

For doing a quick install using LVM (my typical setup), there is a [README](https://gitlab.com/Keydrain/nixconfig/-/blob/main/machines/README.md) that may be useful.

### Users

Each user (meatspace) needs a definition that governs things related to only that user.
Specific files, user groups, programs to install for longer term testing, etc.
Only one user definition can exist per physical machine in the flake.nix file (for now).

### Modules

A module, in this context, is a derivation that determines an application to install and its associated configuration at a system level.
Virtualization, systemd processes, display managers, graphics drivers, etc.
Any number of modules may be added together to define the system level programs present on a machine.

### Programs

A program, in this context, is a set of instructions for home-manager to setup and install an application and its associated configuration at a user level.
Text editors, gui applications, games, cli tools, etc.
Any number of programs may be added together to define the user level programs present on a machine.

### Overlays

Either modifies derivations in this repo or extends derivations by including custom pkgs. Read more: https://nixos.wiki/wiki/Overlays

### Extras

Scripts or extra files related to a module or program. Usually will be symlinked when needed and so a standard reference location is utilized.

## Resources

I didn't come up with all this stuff myself. Needed to learn lots of things. Here are some of the more useful resources that I relied on while setting up this config.

### Nix / NixOS

- Inspiring Nixconfig: ~~https://github.com/btwiusegentoo/nixconfig~~ 404!
- Inspiring Nixos-configs: https://github.com/reckenrode/nixos-configs
- Awesome Nixos Guide: https://ianthehenry.com/posts/how-to-learn-nix/
- Lib Reference for Nix: https://teu5us.github.io/nix-lib.html
- Utils (plus): https://github.com/gytis-ivaskevicius/flake-utils-plus/
- Utils: https://github.com/numtide/flake-utils
- Live CD: https://nixos.wiki/wiki/Creating_a_NixOS_live_CD
- RPI Config: https://nixos.wiki/wiki/NixOS_on_ARM/Raspberry_Pi_4
- RPI Config Discussion: https://mgdm.net/weblog/nixos-on-raspberry-pi-4/
- Flake Utils Plus: https://github.com/viperML/dotfiles
- Overlays Recommendations: https://blog.flyingcircus.io/2017/11/07/nixos-the-dos-and-donts-of-nixpkgs-overlays/

### PCI Passthrough

- PCI Passthrough Wiki Page: https://wiki.archlinux.org/title/PCI_passthrough_via_OVMF
- Arch Guide on KVMGT: https://wiki.archlinux.org/title/Intel_GVT-g
- KVMGT Guide: https://blog.tmm.cx/2020/05/15/passing-an-intel-gpu-to-a-linux-kvm-virtual-machine/
- GVT vBios Rom: https://www.reddit.com/r/VFIO/comments/av736o/creating_a_clover_bios_nonuefi_install_for_qemu/ehdz6mf?utm_source=share&utm_medium=web2x&context=3

### Nix Darwin

- https://github.com/LnL7/nix-darwin
- https://wickedchicken.github.io/post/macos-nix-setup/
- https://markhudnall.com/2021/01/27/first-impressions-of-nix/
- https://github.com/landakram/nix-config
- https://github.com/burke/b
- https://philipp.haussleiter.de/2020/04/fixing-nix-setup-on-macos-catalina

### Nix on Droid

- https://github.com/t184256/nix-on-droid

### K3s

- https://docs.technotim.live/posts/k3s-ha-install/
- https://gabrieltanner.org/blog/ha-kubernetes-cluster-using-k3s
- https://dzone.com/articles/creating-a-highly-available-k3s-cluster
- Kubenix: https://github.com/hall/kubenix
- Multi-Arch Support: https://github.com/dbhi/qus
- K3S Server Configs: https://docs.k3s.io/cli/server

Steps:
- Remove old files or start with new nodes
- Spin up one node with cluster-init
- Extract server token and kubectl config and place in secrets
- Update configs on all servers and start k3s
- Restart initial server after additional control plane nodes spin up
- Deploy GitLab Agent
- Deploy GitLab Runner
- Run deployment pipelines

### Gitlab

- Connecting to the gitlab agent:
  - From a terminal, connect to your cluster and run this command:
  ```
  docker run --pull=always --rm registry.gitlab.com/gitlab-org/cluster-integration/gitlab-agent/cli:stable generate --kas-address=wss://kas.gitlab.com --agent-version stable --namespace gitlab-agent --agent-token=super-secret-token-here | kubectl apply -f -
  ```
  or
  ```
  helm repo add gitlab https://charts.gitlab.io && helm repo update && helm upgrade --install anor-londo gitlab/gitlab-agent --namespace gitlab-agent --set image.tag=v16.5.0 --set config.kasAddress=wss://kas.gitlab.com --set config.token=super-secret-token-here
  ```
- https://about.gitlab.com/blog/2022/04/07/the-ultimate-guide-to-gitops-with-gitlab
- https://docs.gitlab.com/ee/user/clusters/agent/repository.html

### General Stuffs

- Privacy Recommendations: https://privacyguides.org
- Gource script: https://github.com/khuedoan/dotfiles/blob/master/.local/bin/gitviz
- Synology NAS VVM: https://global.download.synology.com/download/Document/Software/WhitePaper/Package/Virtualization/All/enu/Synology_VMM_White_Paper_enu.pdf

## TODO

- dconf2nix for gnome configs https://github.com/Th3Whit3Wolf/nixos/blob/main/hosts/x86_64-linux/tardis/desktop/gnome.nix
- devShell for things https://github.com/numtide/devshell
- Rsync backups example:
  ```
  rsync -Pa --delete -f='P owl.png' ~/git /nfs/abyss/clint/.
  ```
