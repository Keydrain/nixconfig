# Local Path Patch

Local Path storage class should be patched to not be the default solution for storage purposes. This can be done via

```
kubectl patch storageclass local-path -p '{"metadata": {"annotations":{"storageclass.kubernetes.io/is-default-class":"false"}}}'
```

So far, this step is performed manually when the cluster starts up.
<!-- TODO: Figure out a way to automate this -->
