# Agenix Secrets

## Process

1. Add secret file path in secrets.nix
1. `agenix -e file.age`

## Updates

1. `agenix --rekey`

## Create new keys

- Create a user ssh key
```
ssh-keygen -t ed25519 -C "user@machine"
cat id_ed25519.pub
```

- Get machine public key
```
ssh-keyscan machine-name
```

## Documentation

https://github.com/ryantm/agenix
