let
  clint = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIIwcLdfVeWVaNPUw6ZBFtQScgBe1+u8gfCYnQ/fBS7mZ";
  lord-gwyn = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIGh/U3/D7aRJhHNKOvCBRdfc+z9QswOtqsJAjigq4gNg";
  lord-gwynevere = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIL04JudNao28y5DwCuiAfvRW7fxbXC43P0dyoIYV94YP";
  lord-gwyndolin = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIOpNegWqVo1hUsdp+L7z8fJSNErEUEoFJjTZ+3PjZUiG";
  knight-ornstein = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAII/LfjwqPyAzucBZfQfLrsavj7ZpiheN0UxmbQ5KTxQL";
  knight-artorias = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAILmyvrU/8ahexIGbveS3D4s33HdCwypGDysuD/Pmh9rZ";
  knight-ciaran = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIIgc15zmafrvovflIGqF5gLk8phk5DTMHbAskMx5gryL";
  knight-gough = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIDDLL4lJboConjGkl5Yh3y9jaMX91npISBLkdRsvcICb";
  knight-smough = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIFcThsGVFEvcmKtzuUBkRod1Gdq5KgJpUKiEvtV4elCJ";
  lords = [lord-gwyn lord-gwynevere lord-gwyndolin];
  knights = [
    knight-ornstein
    knight-artorias
    knight-ciaran
    knight-gough
    knight-smough
  ];
  users = [clint] ++ lords ++ knights;

  Frampt = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIJPuobAG/INsDM2ZQ5pBg5DculhV0xLmrrrPNDQzgZmJ";
  Gwyn = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIC64b5lRXvVfnjTznipxXFxsJfRtVVxYRZ+iB64DISX1";
  Gwynevere = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIIZacfKiIf1TRdieunepBS2mCPG+CQRdzvclVuawTPJ2";
  Gwyndolin = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAICWa5FijcIbSZWsk07uMNjrgZVJLkuQ/pMSxJD5WmCYi";
  Ornstein = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIGFk8z33raq1xnaLaZQokN8LhAZiTAG2lOY2FKJCQt9t";
  Artorias = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIExMu7FM54P50VErEagyedruFn6iZhlnTNwLOtItIfe3";
  Ciaran = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIKupbNx9mqNwB3Geo4cKPfeqWpyuO8O5xoHKDaglxQ+2";
  Gough = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAINcCba01YZpEoceSJArS4bFf5q/XiQha0aAOdVN/B4ri";
  Smough = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIDUP0refwSkk7rKVEILVPsR4f9bh6TD6gJNHEpqx1yMt";
  nucs = [Gwyn Gwynevere Gwyndolin];
  rpis = [Ornstein Artorias Ciaran Gough Smough];
  systems = [Frampt] ++ nucs ++ rpis;

  k3s = lords ++ knights ++ nucs ++ rpis;
  admin = [clint Frampt];
in {
  "k3s-server-token.age".publicKeys = admin ++ k3s;
  "k3s-gitlab-runner-amd64-secret.age".publicKeys = admin ++ k3s;
  "k3s-gitlab-runner-arm64-secret.age".publicKeys = admin ++ k3s;
  "k3s-gitlab-registry-secret.age".publicKeys = admin ++ k3s;
  "k3s-traefik-dashboard-auth-secret.age".publicKeys = admin ++ k3s;
  "k3s-cert-manager-cloudflare-secret.age".publicKeys = admin ++ k3s;
  "k3s-ddclient-cloudflare-config-secret.age".publicKeys = admin ++ k3s;
  "k3s-foundry-vtt-secret.age".publicKeys = admin ++ k3s;
  "kubectl-config.age".publicKeys = admin;
  "gitlab-agent-token.age".publicKeys = admin;
  "gitlab-runner-amd64-token.age".publicKeys = admin;
  "gitlab-runner-arm64-token.age".publicKeys = admin;
  "gitlab-foundry-vtt-registry-secret.age".publicKeys = admin;
}
