#!/usr/bin/env bash

# Copy original hardware-config to the machine definition
mkdir ~/git/nixconfig/machines/$(hostname)
cp /etc/nixos/hardware-configuration.nix ~/git/nixconfig/machines/$(hostname)/hardware-configuration.nix
chown $(whoami):users ~/git/nixconfig/machines/$(hostname)/hardware-configuration.nix

# Remove existing nixos
sudo rm -rf /etc/nixos

# Link nixconfig as nixos
sudo ln -s $(pwd) /etc/nixos
