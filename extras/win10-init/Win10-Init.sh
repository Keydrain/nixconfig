#!/usr/bin/env bash

echo "Checking Win10 qcow image..."
if [ ! -f ${HOME}/.local/etc/libvirt/Win10.qcow2 ]; then
  echo "Creating a windows 10 qcow2 disk..."
  read -p "Please enter size in Gigs: " size
  qemu-img create -f qcow2 -o cluster_size=2M ${HOME}/.local/etc/libvirt/Win10.qcow2 "$size"G
  chown ${whoami}:users ${HOME}/.local/etc/libvirt/Win10.qcow2 || sudo chown ${whoami}:users ${HOME}/.local/etc/libvirt/Win10.qcow2
fi

echo "Checking virtio image..."
if [ ! -f ${HOME}/.local/etc/libvirt/virtio-win.iso ] || test `find ${HOME}/.local/etc/libvirt/virtio-win.iso -mmin +1440`; then
  echo "Downloading virtio-win.iso..."
  mv -f ${HOME}/.local/etc/libvirt/virtio-win.iso ${HOME}/.local/etc/libvirt/virtio-win.iso.bak 2> /dev/null
  curl --progress-bar -LO --output-dir ${HOME}/.local/etc/libvirt https://fedorapeople.org/groups/virt/virtio-win/direct-downloads/latest-virtio/virtio-win.iso
  chown ${whoami}:users ${HOME}/.local/etc/libvirt/virtio-win.iso || sudo chown ${whoami}:users ${HOME}/.local/etc/libvirt/virtio-win.iso
fi

if [ $1 == "-gvt" ]; then
  echo "Checking GVT vBios ROM..."
  if [ ! -f ${HOME}/.local/etc/libvirt/vbios_gvt_uefi.rom ] || test `find ${HOME}/.local/etc/libvirt/vbios_gvt_uefi.rom -mmin +1440`; then
    echo "Downloading OVMF GVT vBios ROM patch..."
    mv -f ${HOME}/.local/etc/libvirt/vbios_gvt_uefi.rom ${HOME}/.local/etc/libvirt/vbios_gvt_uefi.rom.bak 2> /dev/null
    curl --progress-bar -LO --output-dir ${HOME}/.local/etc/libvirt http://120.25.59.132:3000/vbios_gvt_uefi.rom
    chown ${whoami}:users ${HOME}/.local/etc/libvirt/vbios_gvt_uefi.rom || sudo chown ${whoami}:users ${HOME}/.local/etc/libvirt/vbios_gvt_uefi.rom
  fi
fi

echo "Checking Windows ISO..."
while [ true ]; do
  files=$(ls ${HOME}/.local/etc/libvirt/Win10_*_English_x64.iso 2> /dev/null | wc -l)
  if [ "$files" = "0" ]; then
    echo "Download the English 64-bit windows ISO to ${HOME}/Downloads"
    xdg-open https://www.microsoft.com/en-us/software-download/windows10ISO
    sleep 10
    echo "Press any key once file is downloaded"
    read -n 1
    echo "Attempting to automatically move the downloaded file..."
    mv ${HOME}/Downloads/Win10_*_English_x64.iso ${HOME}/.local/etc/libvirt/
    chown ${whoami}:users ${HOME}/.local/etc/libvirt/Win10_*_English_x64.iso || sudo chown ${whoami}:users ${HOME}/.local/etc/libvirt/Win10_*_English_x64.iso
  else
    break
  fi
done

echo "Creating prebuilt Win10 domains..."
sudo ln -sf ${HOME}/.local/etc/libvirt/Win10.qcow2 /var/lib/libvirt/images/Win10.qcow2
sudo ln -sf ${HOME}/.local/etc/libvirt/virtio-win.iso /var/lib/libvirt/images/virtio-win.iso
if [ $1 == "-gvt" ]; then
  sudo ln -sf ${HOME}/.local/etc/libvirt/vbios_gvt_uefi.rom /var/lib/libvirt/images/vbios_gvt_uefi.rom
fi
for iso in ${HOME}/.local/etc/libvirt/Win10_*_English_x64.iso; do
  sudo ln -sf ${iso} /var/lib/libvirt/images/$(basename ${iso})
done
sudo ln -sf ${HOME}/git/nixconfig/extras/win10-init/Win10-Basic.xml /var/lib/libvirt/qemu/Win10-Basic.xml
if [ -f ${HOME}/git/nixconfig/machines/$(hostname)/Win10.xml ]; then
  sudo ln -sf ${HOME}/git/nixconfig/machines/$(hostname)/Win10.xml /var/lib/libvirt/qemu/Win10.xml
else
  echo "Missing host specific Win10.xml. Please add an xml file for end-state Win10 for this machine at ${HOME}/git/nixconfig/machines/$(hostname)/Win10.xml"
fi

echo "Refreshing libvirtd..."
sudo systemctl try-reload-or-restart libvirtd.service
