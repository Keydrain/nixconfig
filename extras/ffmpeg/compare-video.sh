#!/usr/bin/env bash

if [[ "$1" == "" || "$1" == "help" || "$2" == "" ]]
then
 echo "Usage: `basename $0` video1 video2 [hh:mm:ss]" >&2
 # Error message to stderr.
 exit $E_NOARGS
 # Returns 85 as exit status of script (error code).
fi

if [[ ! -f "$1" || ! -f "$2" ]]; then
 echo "Error: Selected file(s) ($1) are missing"
 exit
fi

if [[ "$1" != *.* ]]; then
 echo "Error: File has no extension (example.mkv)"
 exit
fi

FILE1=$1
FILE2=$2
STIME="00:00:00"

if [[ "$3" == ??:??:?? ]]; then
 STIME=$3
fi

echo "Processing files:"
echo -e "\t input file1: "$FILE1
echo -e "\t input file2: "$FILE2
echo -e "\t starting at: "$STIME

sleep 2

ffplay -i $FILE1 -ss $STIME -vf "[in] scale=iw/2:ih/2, pad=2*iw:ih [left];movie=$FILE2, scale=iw/2:ih/2 [right];[left][right] overlay=main_w/2:0 [out]"
