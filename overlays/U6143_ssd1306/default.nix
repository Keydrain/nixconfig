{
  inputs,
  pkgs,
  stdenv,
  lib,
  ...
}:
stdenv.mkDerivation rec {
  pname = "U6143_ssd1306";
  version = "20.0"; # No official version number, so just going with commits
  src = inputs.U6143_ssd1306;
  buildInputs = with pkgs; [gnumake];

  buildPhase = ''
    cd C
    make
  '';

  installPhase = ''
    mkdir -p $out/bin
    mv display $out/bin
  '';

  meta = with lib; {
    description = "PiOLED display by UCTRONICS";
    longDescription = ''
      Boot-up script to read stats (CPU/Memory load, IP address, temperature, etc.)
      and display these metrics on the UCTRONICS PiOLED display.
    '';
    homepage = "https://www.uctronics.com/";
    platforms = ["aarch64-linux"]; # Only tested on raspberry pi
  };
}
