# NVMe LVM Setup

This script is for setting up an initial nixos install. Do not run on anything that may have data you wish to save.

Please refer to https://nixos.org/manual/nixos/stable/ for additional details.

## Clear disk
```
parted /dev/nvme0n1 -- mklabel gpt
```

### Clear isk with lvm records

If the volume already contains an LVM group `lvscan`, perform these steps first:

#### Deactivate Logical Volumes
```
lvchange -an /dev/Group/Volume1
lvchange -an /dev/Group/Volume2
...
```

#### Remove Logical Volumes
```
lvremove /dev/Group/Volume1
lvremove /dev/Group/Volume2
...
```

#### Remove Volume Group
```
vgremove Group
```

#### Remove Physical Volume
```
pvremove /dev/nvme0n1p2
```

## Setup partitions
```
parted /dev/nvme0n1 -- mkpart ESP fat32 1MiB 512MiB
parted /dev/nvme0n1 -- mkpart primary 512MiB 100%
parted /dev/nvme0n1 -- set 1 esp on
parted /dev/nvme0n1 -- set 2 lvm on
```

## Format partitions
```
mkfs.fat -F 32 -n boot /dev/nvme0n1p1
pvcreate /dev/nvme0n1p2
parted /dev/nvme0n1 -- name 2 'LVM'
```

## Create Volume Group
```
vgcreate Disk /dev/nvme0n1p2
```

## Create Logical Volumes
```
lvcreate -L 100G Disk -n root
lvcreate -l 100%FREE Disk -n home
```

## Format Logical Volumes
```
mkfs.ext4 -L root /dev/mapper/Disk-root
mkfs.ext4 -L home /dev/mapper/Disk-home
```

## Mount file systems
```
mount /dev/disk/by-label/root /mnt

mkdir /mnt/boot
mkdir /mnt/home

mount /dev/disk/by-label/boot /mnt/boot
mount /dev/disk/by-label/home /mnt/home
```

## Generate Configs
```
nixos-generate-config --root /mnt
```

Edit `/mnt/etc/nixos/configuration.nix` as needed.
- Edit timezone
- Enable touch support if needed
- Add `git`, `vim`, & `firefox` to list of packages
- Edit/add user
- Set the correct hostname
- Enable flakes
  ```
  { pkgs, ... }: {
    nix = { 
      package = pkgs.nixUnstable;
      extraOptions = ''
        experimental-features = nix-command flakes
      '';
    };
  }
  ```

## Install
```
nixos-install
```

## After Reboot

- Setup password for the user
- Remove the root user
  ```
  passwd -d root
  passwd --lock root
  ```
- Update the machine hardware-configuration.nix before rebuilding again
- Add git tokens for automated updates
  ```
  ssh-keygen -t ed25519 -C "<comment>"
  ```
