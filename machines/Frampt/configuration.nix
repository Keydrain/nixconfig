{
  pkgs,
  lib,
  ...
}: {
  # Use the systemd-boot EFI boot loader.
  boot = {
    binfmt.emulatedSystems = [
      "aarch64-linux"
      # "i686-linux"
      # "x86_64-windows"
    ]; # Needed for cross-compiling
    kernelModules = ["kvmgt" "vfio-iommu-type1" "vfio-mdev" "tun"];
    kernelPackages = lib.mkDefault pkgs.linuxPackages_latest;
    kernelParams = ["intel_iommu=on" "i915.enable_gvt=1" "i915.enable_guc=0"];
    kernel.sysctl."dev.i915.perf_stream_paranoid" = 0;
    loader = {
      efi.canTouchEfiVariables = true;
      systemd-boot = {
        enable = true;
        editor = false;
        configurationLimit = 9;
        memtest86.enable = true;
      };
      timeout = 10;
    };
  };

  # Remove default packages that aren't needed
  environment.defaultPackages = lib.mkForce [];

  # Match number of jobs to number of cores
  nix.settings.max-jobs = lib.mkDefault 8;

  # The global useDHCP flag is deprecated, therefore explicitly set to false here.
  # Per-interface useDHCP will be mandatory in the future, so this generated config replicates the default behaviour.
  networking = {
    useDHCP = false;
    interfaces.wlp58s0.useDHCP = true;
  };

  # Enable rpcbind for nfs
  services.rpcbind.enable = true;

  # Set your time zone.
  time.timeZone = "America/Denver";

  # Enable package documentation
  documentation.enable = true;

  # Make CapsLock the Esc key.
  console.useXkbConfig = true;
  services.xserver.xkb.options = "caps:escape";

  # Enable CUPS to print documents.
  services.printing.enable = true;
  services.printing.drivers = with pkgs; [
    master.gutenprint
    master.cnijfilter2
    hplip
    hplipWithPlugin
  ];
  hardware.sane.enable = true;
  hardware.sane.extraBackends = with pkgs; [hplipWithPlugin];
  # This causes a problem https://wiki.archlinux.org/title/CUPS/Troubleshooting#Conflict_with_SANE or https://wiki.archlinux.org/title/CUPS/Troubleshooting#Bad_permissions

  # Add /etc files
  environment.etc = {};

  # KVMGT virtualization
  virtualisation.kvmgt.enable = true;

  systemd.services = {
    kvmgt-setup = {
      description = "Setup KVMGT";
      enable = true;
      serviceConfig = {Type = "oneshot";};
      environment.SHELL = "/run/current-system/sw/bin/bash";
      script = ''
        if [ ! -d /sys/devices/pci0000:00/0000:00:02.0/bff704a7-5caf-5ea1-a6c5-4e65f49f1894 ]; then
          echo bff704a7-5caf-5ea1-a6c5-4e65f49f1894 > /sys/devices/pci0000:00/0000:00:02.0/mdev_supported_types/i915-GVTg_V5_4/create
        fi
      '';
      wantedBy = ["multi-user.target"];
    };
    NetworkManager-wait-online.enable = false;
  };

  # Make nix more secure
  nix.settings.allowed-users = ["@wheel"];
  security.sudo.execWheelOnly = true;
  # security.auditd.enable = true; # TODO: Learn more
}
