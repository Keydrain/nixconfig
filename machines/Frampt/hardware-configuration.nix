# Do not modify this file!  It was generated by ‘nixos-generate-config’
# and may be overwritten by future invocations.  Please make changes
# to /etc/nixos/configuration.nix instead.
{
  config,
  lib,
  pkgs,
  modulesPath,
  variables,
  ...
}: {
  imports = [(modulesPath + "/installer/scan/not-detected.nix")];

  boot.initrd.availableKernelModules = ["xhci_pci" "nvme" "usbhid" "rtsx_pci_sdmmc"];
  boot.initrd.kernelModules = ["dm-snapshot"];
  boot.kernelModules = ["kvm-intel"];
  boot.extraModulePackages = [];

  fileSystems."/" = {
    device = "/dev/disk/by-uuid/b8378726-d58e-42a0-ab34-ef01e866766c";
    fsType = "ext4";
  };

  fileSystems."/boot" = {
    device = "/dev/disk/by-uuid/5A8C-DD43";
    fsType = "vfat";
  };

  fileSystems."/home" = {
    device = "/dev/disk/by-uuid/d96483d8-4ca6-4358-aff8-504001d6ce1a";
    fsType = "ext4";
  };

  fileSystems."/nfs/abyss/${variables.primaryUser}" = {
    device = "172.24.0.2:/volume1/${variables.primaryUser}";
    fsType = "nfs";
    options = [
      "user"
      "x-systemd.automount"
      "noauto"
      "x-systemd.idle-timeout=600"
      "_netdev"
      "nofail"
    ];
  };

  fileSystems."/nfs/abyss/k3s" = {
    device = "172.24.0.2:/volume1/k3s";
    fsType = "nfs";
    options = [
      "user"
      "x-systemd.automount"
      "noauto"
      "x-systemd.idle-timeout=600"
      "_netdev"
      "nofail"
    ];
  };

  swapDevices = [
    {
      device = "/home/swapfile";
      size = 1024 * 16;
    }
  ];

  powerManagement.cpuFreqGovernor = lib.mkDefault "powersave";
}
