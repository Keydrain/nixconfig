{
  # Define the system hostname.
  networking.hostName = "Frampt";

  # If a uuid is needed for this system:
  #
  # uuidgen -N Frampt -n @oid -s
  #
  # db85d8eb-a95e-53c0-ac45-117e9ad760fc
}
