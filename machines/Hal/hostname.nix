{
  # Define the system hostname.
  networking.hostName = "Hal";

  # If a uuid is needed for this system:
  #
  # uuidgen -N Hal -n @oid -s
  #
  # b1fffb18-1b85-56a5-8b86-15ba39759087
}
