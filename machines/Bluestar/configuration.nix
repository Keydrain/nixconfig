{
  pkgs,
  lib,
  ...
}: {
  # Use the systemd-boot EFI boot loader.
  boot = {
    kernelPackages = lib.mkDefault pkgs.linuxPackages_latest;
    loader = {
      systemd-boot = {
        enable = true;
        editor = false;
        configurationLimit = 18;
        memtest86.enable = true;
      };
      efi.canTouchEfiVariables = true;
    };
  };

  # Remove default packages that aren't needed
  environment.defaultPackages = lib.mkForce [];

  # Match number of jobs to number of cores
  nix.settings.max-jobs = lib.mkDefault 8;

  # The global useDHCP flag is deprecated, therefore explicitly set to false here.
  # Per-interface useDHCP will be mandatory in the future, so this generated config replicates the default behaviour.
  networking = {
    useDHCP = false;
    interfaces.wlp58s0.useDHCP = true;
  };

  # Enable rpcbind for nfs
  services.rpcbind.enable = true;

  # Set your time zone.
  time.timeZone = "America/Denver";

  # Enable package documentation
  documentation.enable = true;

  # Enable CUPS to print documents.
  services.printing.enable = true;
  services.printing.drivers = with pkgs; [
    master.gutenprint
    master.cnijfilter2
    hplip
    hplipWithPlugin
  ];

  # Add /etc files
  environment.etc = {};

  # Make nix more secure
  nix.settings.allowed-users = ["@wheel"];
  security.sudo.execWheelOnly = true;
}
