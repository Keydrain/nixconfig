{
  # Define the system hostname.
  networking.hostName = "Bluestar";

  # If a uuid is needed for this system:
  #
  # uuidgen -N Bluestar -n @oid -s
  #
  # 6d62d38d-f1b7-56de-8b94-4f72ae8b0957
}
