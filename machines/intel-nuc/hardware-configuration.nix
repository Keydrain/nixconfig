# This file is a stub. The real hardware config will replace it upon successful generation.
{
  config,
  lib,
  pkgs,
  modulesPath,
  variables,
  ...
}: {
  imports = [(modulesPath + "/installer/scan/not-detected.nix")];

  boot.initrd.availableKernelModules = [
    "xhci_pci"
    "thunderbolt"
    "ahci"
    "nvme"
    "usb_storage"
    "usbhid"
    "sd_mod"
    "rtsx_pci_sdmmc"
  ];
  boot.initrd.kernelModules = ["dm-snapshot"];
  boot.kernelModules = ["kvm-intel"];
  boot.extraModulePackages = [];

  fileSystems."/" = {
    device = "/dev/disk/by-label/root";
    fsType = "ext4";
  };

  fileSystems."/boot" = {
    device = "/dev/disk/by-label/boot";
    fsType = "vfat";
  };

  fileSystems."/nfs/abyss/${variables.primaryUser}" = {
    device = "172.24.0.2:/volume1/${variables.primaryUser}";
    fsType = "nfs";
    options = ["hard" "user" "_netdev" "nofail"];
  };

  fileSystems."/nfs/abyss/k3s" = {
    device = "172.24.0.2:/volume1/k3s";
    fsType = "nfs";
    options = ["hard" "user" "_netdev" "nofail"];
  };

  powerManagement.cpuFreqGovernor = lib.mkDefault "powersave";
  hardware.cpu.intel.updateMicrocode =
    lib.mkDefault config.hardware.enableRedistributableFirmware;
}
