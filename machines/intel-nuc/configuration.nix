{
  pkgs,
  lib,
  variables,
  ...
}: {
  boot = {
    binfmt.emulatedSystems = ["aarch64-linux"]; # Needed for cross-compiling
    loader = {
      efi.canTouchEfiVariables = true;
      systemd-boot = {
        enable = true;
        editor = false;
        configurationLimit = 5;
      };
    };
  };

  # Remove default packages that aren't needed
  environment.defaultPackages = lib.mkForce [];

  # Match number of jobs to number of cores
  # Don't actually do this, instead set it to one core so other active processes aren't starved
  nix.settings.max-jobs = lib.mkForce 1;

  # The global useDHCP flag is deprecated, therefore explicitly set to false here.
  # Per-interface useDHCP will be mandatory in the future, so this generated config replicates the default behaviour.
  networking = {
    useDHCP = true;
    interfaces.wlo1.useDHCP = true;
    wireless.enable = lib.mkForce false;
  };

  # Enable rpcbind for nfs
  services.rpcbind.enable = true;

  # Set your time zone.
  time.timeZone = "America/Denver";

  # Disable package documentation
  documentation.enable = false;

  # Make CapsLock the Esc key.
  console.useXkbConfig = true;
  services.xserver.xkb.options = "caps:escape";

  # Make nix more secure
  nix.settings.allowed-users = ["@wheel"];
  security.sudo.execWheelOnly = true;
}
