{lib, ...}: {
  # Define the system hostname.
  networking.hostName = lib.mkDefault "intel-nuc";
}
