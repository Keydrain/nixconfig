# This file is a stub. The real hardware config will replace it upon successful generation.
{
  config,
  lib,
  pkgs,
  modulesPath,
  variables,
  ...
}: {
  imports = [(modulesPath + "/installer/scan/not-detected.nix")];

  boot.initrd.availableKernelModules = ["xhci_pci"];
  boot.initrd.kernelModules = [];
  boot.kernelModules = [];
  boot.extraModulePackages = [];

  fileSystems."/" = {
    device = "/dev/disk/by-label/NIXOS_SD";
    fsType = "ext4";
    options = ["noatime"];
  };

  fileSystems."/nfs/abyss/${variables.primaryUser}" = {
    device = "172.24.0.2:/volume1/${variables.primaryUser}";
    fsType = "nfs";
    options = ["hard" "user" "_netdev" "nofail"];
  };

  fileSystems."/nfs/abyss/k3s" = {
    device = "172.24.0.2:/volume1/k3s";
    fsType = "nfs";
    options = ["hard" "user" "_netdev" "nofail"];
  };

  powerManagement.cpuFreqGovernor = lib.mkDefault "performance";
}
