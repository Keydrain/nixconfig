{
  pkgs,
  lib,
  variables,
  ...
}: {
  hardware.raspberry-pi."4" = {
    poe-hat.enable = true;
    i2c1.enable = true;
  };
  users.users.${variables.primaryUser} = {extraGroups = ["i2c"];};

  boot = {
    binfmt.emulatedSystems = ["x86_64-linux"]; # Needed for cross-compiling
    kernelPackages = pkgs.linuxPackages_rpi4;
    kernelParams = ["8260.nr_uarts=1" "cgroup_enable=cpuset" "cgroup_memory=1" "cgroup_enable=memory"];
    loader = {
      generic-extlinux-compatible.enable = true;
      grub.enable = false;
    };
  };

  # Remove default packages that aren't needed
  environment.defaultPackages = lib.mkForce [];

  # Match number of jobs to number of cores
  # Don't actually do this, instead set it to one core so other active processes aren't starved
  nix.settings.max-jobs = lib.mkForce 1;

  # The global useDHCP flag is deprecated, therefore explicitly set to false here.
  # Per-interface useDHCP will be mandatory in the future, so this generated config replicates the default behaviour.
  networking = {
    useDHCP = true;
    interfaces.eth0.useDHCP = true;
    interfaces.wlan0.useDHCP = true;
    wireless.enable = lib.mkForce false;
  };

  # Enable rpcbind for nfs
  services.rpcbind.enable = true;

  # Set your time zone.
  time.timeZone = "America/Denver";

  # Disable package documentation
  documentation.enable = false;

  # Make CapsLock the Esc key.
  console.useXkbConfig = true;
  services.xserver.xkb.options = "caps:escape";

  # Make nix more secure
  nix.settings.allowed-users = ["@wheel"];
  security.sudo.execWheelOnly = true;
}
