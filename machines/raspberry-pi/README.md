# RPI cluster machine

NOTE: Interesting possible approach: https://github.com/Robertof/nixos-docker-sd-image-builder

This host is the base for RPI clusters. It is defined as both a host and a package install.

Setup the sd card:
```
nix build .#raspberry-pi-installer --verbose --system aarch64-linux
unzstd result/sd-image/nixos-sd-image... -o ~/some/path/rpi.img
```

Steps after boot:
- Install the flake:
  ```
  git clone https://gitlab.com/Keydrain/nixconfig.git
  cd nixconfig
  sudo nixos-rebuild boot --verbose --flake ".#Knight-Name-Here"
  ```
- Reboot
- Setup git flake
  ```
  mkdir git
  cd git
  git clone https://gitlab.com/Keydrain/nixconfig.git
  cd nixconfig
  sudo rm -rf /etc/nixos
  sudo ln -s $(pwd) /etc/nixos
  sudo nixos-rebuild switch --verbose
  ```
- Change knight password
  ```
  passwd
  passwd -d root
  passwd --lock root
  ```
