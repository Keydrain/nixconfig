{lib, ...}: {
  # Define the system hostname.
  networking.hostName = lib.mkDefault "nixos";

  # If a uuid is needed for this system:
  #
  # uuidgen -N nixos -n @oid -s
  #
  # f607f983-551c-5bcf-8e82-98fff04fd3ed
}
