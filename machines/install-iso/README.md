# ISO for custom NixOS installs

To build this, for example:
```nix build .#install-iso --verbose --system aarch64-linux```

ISO file will be in `results/iso/...`
