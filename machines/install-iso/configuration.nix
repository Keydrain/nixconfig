{lib, ...}: {
  # Match number of jobs to number of cores
  nix.settings.max-jobs = lib.mkDefault 4;

  # The global useDHCP flag is deprecated, therefore explicitly set to false here.
  # Per-interface useDHCP will be mandatory in the future, so this generated config replicates the default behaviour.
  networking = {
    useDHCP = false;
    interfaces.eth0.useDHCP = true;
    interfaces.wlan0.useDHCP = true;
    wireless.enable = lib.mkForce false;
  };

  # Enable ssh
  services.openssh.enable = true;

  # Set your time zone.
  time.timeZone = "America/Denver";

  # Enable package documentation
  documentation.enable = true;

  # Make CapsLock the Esc key.
  console.useXkbConfig = lib.mkDefault true;
  services.xserver.xkb.options = lib.mkDefault "caps:escape";

  # Make nix more secure
  nix.settings.allowed-users = ["@wheel"];
  security.sudo.execWheelOnly = true;
}
