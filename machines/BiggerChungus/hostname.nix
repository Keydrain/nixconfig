{
  # Define the system hostname.
  networking.hostName = "BiggerChungus";

  # If a uuid is needed for this system:
  #
  # uuidgen -N BiggerChungus -n @oid -s
  #
  # 278fc61e-390a-504c-b772-785deb778f2f
}
