{
  pkgs,
  lib,
  ...
}: {
  # Use the systemd-boot EFI boot loader.
  boot = {
    kernelModules = ["vfio_pci" "vfio" "vfio_iommu_type1" "vfio_virqfd" "tun"];
    # kernelPackages = lib.mkDefault pkgs.linuxPackages_latest;
    kernelParams = ["vfio-pci.ids=10de:1b80,10de:10f0" "intel_iommu=on"];
    loader = {
      efi.canTouchEfiVariables = true;
      systemd-boot = {
        enable = true;
        editor = false;
        configurationLimit = 3;
      };
      timeout = 10;
    };
  };

  # Remove default packages that aren't needed
  environment.defaultPackages = lib.mkForce [];

  # Match number of jobs to number of cores
  nix.settings.max-jobs = lib.mkDefault 12;

  # The global useDHCP flag is deprecated, therefore explicitly set to false here.
  # Per-interface useDHCP will be mandatory in the future, so this generated config replicates the default behaviour.
  networking = {
    useDHCP = false;
    interfaces.eno1.useDHCP = true;
    interfaces.enp12s0.useDHCP = true;
  };

  # Enable rpcbind for nfs
  services.rpcbind.enable = true;

  # Set your time zone.
  time.timeZone = "America/Denver";

  # Enable package documentation
  documentation.enable = true;

  # Enable CUPS to print documents.
  services.printing.enable = true;
  services.printing.drivers = with pkgs; [gutenprint hplip hplipWithPlugin];
  hardware.sane.enable = true;
  hardware.sane.extraBackends = with pkgs; [hplipWithPlugin];

  # Make nix more secure
  nix.settings.allowed-users = ["@wheel"];
  security.sudo.execWheelOnly = true;
}
